package de.htwsaar.vssoap.client.exception;

/**
 * @author cedosw (28.03.2017)
 */
public class MessageException extends Exception {
    public MessageException(String msg) {
        super(msg);
    }

    public MessageException(String msg, Exception cause) {
        super(msg, cause);
    }
}
