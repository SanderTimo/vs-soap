package de.htwsaar.vssoap.client.view;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import de.htwsaar.vssoap.client.Router;
import de.htwsaar.vssoap.client.exception.LoginException;
import de.htwsaar.vssoap.client.exception.ValidationException;
import de.htwsaar.vssoap.client.transmitter.ConnectionTransmitter;
import de.htwsaar.vssoap.client.transmitter.LoginTransmitter;
import de.htwsaar.vssoap.parent.ConnectionConfig;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.WebServiceException;
import org.springframework.ws.client.WebServiceIOException;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author cedosw
 */
public class LoginController implements Initializable {


    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);


    @Autowired
    private LoginTransmitter client;

    @Autowired
    private Router router;

    @Autowired
    ConnectionConfig connection;

    @Autowired
    ConnectionTransmitter transmitter;

    @FXML
    private JFXTextField usernameInput;
    @FXML
    private JFXPasswordField passwordInput;
    @FXML
    private JFXTextField hostInput;

    public void initialize(URL url, ResourceBundle bundle) {
    }

    @FXML
    protected void login() {
        //TODO host in ConnectionConfig.setParentUrl eintragen
        //TODO Message an host, Registrierung des clients fuer zukuenftige updates
        try {
            if (hostInput.getText().isEmpty() || !hostInput.getText().startsWith("http://")) {
                throw new ValidationException("Host cannot be empty and has to start with 'http://'.");
            }
            connection.setParentUrl(hostInput.getText().trim());
            connection.setCredentials(usernameInput.getText(), passwordInput.getText());
            if (client.login(usernameInput.getText(), passwordInput.getText())) {

                router.getStage().setResizable(false);
                router.setSceneContent("/main.fxml");
//                Toast.makeMessage(router.getStage(), "Login Success!", 2500, 250, 250);
                transmitter.sendConnectionRequest(hostInput.getText().trim());


            } else {
                ToastView.makeErrorMessage(router.getStage(), "Login failed! Please check username or password", 2500, 250, 250);
            }
        } catch (ValidationException e) {
            ToastView.makeErrorMessage(router.getStage(), e.getLocalizedMessage(), 2500, 250, 250);
        } catch (LoginException | WebServiceException e) {
            ToastView.makeErrorMessage(router.getStage(), "Cannot connect to server! Pls verify Host or Connection.", 2500, 250, 250);
        } catch (Exception e) {
            logger.error(e.toString());
            e.printStackTrace();
        }
    }

    @FXML
    protected void exit() {
        System.exit(0); //TODO Ordentlicher Exit.
    }
}
