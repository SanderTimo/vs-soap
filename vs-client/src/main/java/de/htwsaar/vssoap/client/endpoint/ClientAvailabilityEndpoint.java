package de.htwsaar.vssoap.client.endpoint;

import de.htwsaar.vssoap.domain.GetAvailabilityRequest;
import de.htwsaar.vssoap.domain.GetAvailabilityResponse;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 * Created by Timo on 25.03.2017.
 */
@Endpoint
public class ClientAvailabilityEndpoint {
    private static final String NAMESPACE_URI = "http://htwsaar.de/vssoap/domain";
    
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAvailabilityRequest")
    @ResponsePayload
    public GetAvailabilityResponse getMessage(@RequestPayload GetAvailabilityRequest request) {

        GetAvailabilityResponse response = new GetAvailabilityResponse();
        response.setAvailability(true);
        return response;
    }

}

