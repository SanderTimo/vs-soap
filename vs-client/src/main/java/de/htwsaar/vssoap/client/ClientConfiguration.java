package de.htwsaar.vssoap.client;

import de.htwsaar.vssoap.client.view.LoginController;
import de.htwsaar.vssoap.client.view.MainController;
import de.htwsaar.vssoap.client.view.SetParentController;
import de.htwsaar.vssoap.parent.SharedTransmitterConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;

/**
 * @author cedosw
 */
@Configuration
@Import(SharedTransmitterConfiguration.class)
public class ClientConfiguration {

    @Bean
    @Scope("prototype")
    LoginController loginController() {
        return new LoginController();
    }

    @Bean
    @Scope("prototype")
    SetParentController setParentController() {
        return new SetParentController();

    }
    @Bean
    MainController mainController() {
            return new MainController();
    }
}
