package de.htwsaar.vssoap.client.messaging;

import de.htwsaar.vssoap.client.view.MainController;
import de.htwsaar.vssoap.domain.CreateMessageResponse;
import de.htwsaar.vssoap.domain.GetAvailabilityResponse;
import de.htwsaar.vssoap.domain.Message;
import de.htwsaar.vssoap.parent.AvailabilityTransmitter;
import de.htwsaar.vssoap.parent.ConnectionConfig;
import de.htwsaar.vssoap.parent.MessageClient;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.WebServiceIOException;

import javax.annotation.PostConstruct;

/**
 * Created by Timo on 28.03.2017.
 */
@Component
public class SendMessageTask {
    private static Logger logger = LoggerFactory.getLogger(SendMessageTask.class);

    @Autowired
    private ConnectionConfig connection;
    @Autowired
    private MessageClient transmitter;

    @Autowired
    AvailabilityTransmitter availabilityTransmitter;

    @Autowired
    private MainController mainController;

    private ObservableList<Message> errorMessageList = FXCollections.synchronizedObservableList(FXCollections.observableArrayList());
    private  Thread sendErrorMessageThread = null;


    /**
     * Sendet die message an den zugehörigen Server
     * Beim fehler wird die Nachricht in die errorListe eingefügt
     * @param message
     */
    public void sendMessage(Message message){
        logger.info("Neue Message zum Absenden aufgenommen.");
        try {
            CreateMessageResponse response = (CreateMessageResponse) transmitter.sendMessageToUri(message, connection.getParentUrl());
            if (response.getMessage().getMessageText().contains("Beim Senden der Message ist ein Fehler aufgetreten")) {
                addMessageToTask(message);
            }
        }catch(WebServiceIOException e){
            addMessageToTask(message);
        }
    }

    private void addMessageToTask(Message message) {
        logger.info("Keine Verbindung zum Server");
        errorMessageList.add(message);
        sendMessage();
    }

    /**
     * Überprüft ob die Connection zum Server steht und arbeitet die ErrorListe einzeln ab
     */
    private void sendMessage() {

        if(sendErrorMessageThread == null) {  // nur ein thread gleichzeitig
            sendErrorMessageThread = new Thread(
                    new Task() {
                        @Override
                        protected Object call() throws Exception {

                            while (!errorMessageList.isEmpty()) {

                                checkConnection();
                                if (checkConnection()) {
                                    try {
                                        logger.info("Sende Message");
                                        CreateMessageResponse response = (CreateMessageResponse) transmitter.sendMessageToUri(errorMessageList.get(0), connection.getParentUrl());
                                        errorMessageList.remove(0);
                                        Platform.runLater(() -> mainController.updatePendingMessage(errorMessageList.size()));
                                    } catch (WebServiceIOException e) {

                                    }
                                } else {
                                    Thread.sleep(2000);
                                }
                            }
                            return true;

                        }
                    });
            sendErrorMessageThread.setDaemon(true);
            sendErrorMessageThread.start();
        }
    }

    public boolean checkConnection(){
        GetAvailabilityResponse response = (GetAvailabilityResponse)availabilityTransmitter.sendAvailabilityRequestToUri(connection.getParentUrl());
        if(response.isAvailability()){
            return true;
        }else{
             logger.info("Server nicht erreichbar");
            Platform.runLater(() -> mainController.updatePendingMessage(errorMessageList.size()));

             return false;
        }
    }

}
