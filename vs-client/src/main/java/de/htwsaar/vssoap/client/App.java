package de.htwsaar.vssoap.client;

import de.htwsaar.vssoap.parent.AbstractJavaFxApp;
import de.htwsaar.vssoap.parent.ConnectionConfig;
import javafx.application.Preloader;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Lazy;

/**
 * Die Main Klasse der Applikation. Erbt von der abstrakten JavaFx-Oberklasse um Spring-Support
 * auch in JavaFX-Klassen zu gewährleisten.
 *
 * @author cedosw
 */
@Lazy
@SpringBootApplication
@ComponentScan({"de.htwsaar.vssoap.parent","de.htwsaar.vssoap.client" })
public class App extends AbstractJavaFxApp {

    private final Logger logger = LoggerFactory.getLogger(App.class);

    @Value("${ui.client.title}")
    private String windowClientTitle;

    @Autowired
    Router router;

    @Autowired
    ConnectionConfig connection;

    /**
     * Startet die JavaFX Stage.
     *
     * @param stage Die MainStage der Anwendung.
     * @throws Exception Exceptions während dem Starten der JavaFX Stage.
     */
    @Override
    public void start(Stage stage) throws Exception {
        notifyPreloader(new Preloader.StateChangeNotification(Preloader.StateChangeNotification.Type.BEFORE_START));
        logger.info(connection.getUrl());
        router.setStage(stage);
        router.setSceneContent("/login.fxml", "/theme.css", 500, 300);

        stage.setTitle(windowClientTitle);
        stage.setResizable(true);
        stage.centerOnScreen();
        stage.show();
        stage.getIcons().add(new Image("/soap.png"));
        router.setStage(stage);
    }

    /**
     * Die Main Methode des Programms.
     *
     * @param args Kommandozeilenparameter.
     */
    public static void main(String args[]) {
        launchApp(App.class, args);
    }
}