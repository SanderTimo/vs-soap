package de.htwsaar.vssoap.client.view;

import com.jfoenix.controls.*;
import de.htwsaar.vssoap.client.Router;
import de.htwsaar.vssoap.client.messaging.SendMessageTask;
import de.htwsaar.vssoap.domain.Message;
import de.htwsaar.vssoap.parent.ActionType;
import de.htwsaar.vssoap.parent.ConnectionConfig;
import de.htwsaar.vssoap.parent.PermissionType;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import de.htwsaar.vssoap.client.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.net.URL;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;

@Component
public class MsgWindowController implements Initializable {
    @Autowired
    private Router router;

    @Autowired
    private Session session;

    @Autowired
    private ConnectionConfig connectionConfig;

    @Autowired
    SendMessageTask sendMessageTask;

    @FXML
    JFXCheckBox cb_PrivateButton;

    @FXML
    JFXTextArea messageArea;
    @FXML
    JFXButton sendButton;
    @FXML
    JFXButton clearButton;
    @FXML
    JFXTextField subjectArea;
    @FXML
    JFXComboBox rangBox;

    Message message;
    ObservableList<String> ranglist = FXCollections.observableArrayList();

    private boolean serverConnected;

    @FXML
    JFXButton sendMessage;
    boolean selected = false;

    public MsgWindowController() {}



    @Override
    public void initialize(URL url, ResourceBundle rb){
       int rang  = connectionConfig.getRang();
       ranglist.clear();
       for(int i = rang; i>=0; i--)
       {
           rang = i;
           ranglist.add(String.valueOf(rang));
       }
       rangBox.setItems(ranglist);
       rangBox.getSelectionModel().selectFirst();
       cb_PrivateButton.setDisable(false);



        //annahme das die ränge absteigend sortiert sind
        rangBox.setOnAction((e) ->{
            if(rangBox.getSelectionModel().getSelectedIndex()!=0){
                cb_PrivateButton.setDisable(true);
                cb_PrivateButton.setSelected(false);
            } else {
                cb_PrivateButton.setDisable(false);
            }
        });




        sendButton.setOnAction(e -> {
            if(messageArea.getText().isEmpty() || subjectArea.getText().isEmpty()){
                ToastView.makeErrorMessage(router.getStage(), "Please enter both a subject and a message body.",2000, 200, 200);
                //setzen des focus nimmt focus vom button, sodass die farbe nicht auf der hover-farbe bleibt
                subjectArea.requestFocus();
                return;
            }

            message = new Message();
            message.setSubject(subjectArea.getText());
            message.setMessageText(messageArea.getText());
            message.setOwner(session.getUsername());

            message.setActionType(ActionType.SAVE.toString());
            message.setDepartment(session.getDepartment());



            String chosenRank;
            if(rangBox.getValue()!=null) {
                chosenRank=(String) rangBox.getValue();
            }
            else chosenRank=String.valueOf(connectionConfig.getRang());
            message.setRang(Integer.valueOf(chosenRank));
            message.setType((cb_PrivateButton.isSelected() && !cb_PrivateButton.isDisable()) ? PermissionType.PRIVATE.toString() : PermissionType.PUBLIC.toString());


           if(!session.isAdmin()){
               if(message.getRang()<connectionConfig.getRang()) {
                   message.setApprovalNeeded(true);
               }
           } else {
               message.setApprovalNeeded(false);
           }
            try {
                GregorianCalendar c = new GregorianCalendar();
                c.setTime(new Date());
                message.setUpdatedAt(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
            }catch (DatatypeConfigurationException dataTypeException){
                //message date cannot be updated for unknown reason
            }
            addMessageToTaskList(message);

            router.closeModal((Parent) ((JFXButton) e.getTarget()).getParent());


        });
        clearButton.setOnAction(e -> {
            cb_PrivateButton.setSelected(false);
            subjectArea.clear();
            messageArea.clear();
        });


    }

    public boolean isServerConnected() {
        return serverConnected;
    }


    public void addMessageToTaskList(Message message) {
        sendMessageTask.sendMessage(message);
    }
}
