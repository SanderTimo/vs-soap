package de.htwsaar.vssoap.client.transmitter;

/**
 * Klasse zum Absenden von Messages zum Anmelden als Kindserver/Client
 *
 * @author JulianW
 */


import de.htwsaar.vssoap.domain.ParentConnectionRequest;
import de.htwsaar.vssoap.domain.ParentConnectionResponse;
import de.htwsaar.vssoap.parent.AbstractTransmitter;
import de.htwsaar.vssoap.parent.ConnectionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.ws.WebServiceException;
import org.springframework.ws.client.WebServiceTransportException;
import org.springframework.ws.transport.http.HttpTransportConstants;

import java.math.BigInteger;

@Component
public class ConnectionTransmitter extends AbstractTransmitter {

    private static final Logger logger = LoggerFactory.getLogger(ConnectionTransmitter.class);

    public ParentConnectionResponse sendConnectionRequest(String parentUrl) {
        ParentConnectionRequest request = new ParentConnectionRequest();
        request.setType(ConnectionType.CLIENT.toString());
        request.setChildUrl(getConnection().getUrl());
        request.setParentUrl(parentUrl);
        //sending request
        ParentConnectionResponse response;

        //handling answer
        try {
            response = sendRequest(request);
            logger.info(response.toString());
            getConnection().setParentUrl(response.getParentUrl());
            getConnection().setRang(response.getRang().intValue());

        } catch (WebServiceTransportException wst) {
            //MessagePersistence senden fehlgeschlagen bzw. keine Antwort erhalten
            //reschedulen?
            response = new ParentConnectionResponse();
            response.setRang(new BigInteger(Integer.toString(HttpTransportConstants.STATUS_NOT_FOUND)));
            logger.error("Could not reach parent server");
            logger.error(wst.toString());
            wst.printStackTrace();
        }
        catch (WebServiceException wse){
            //MessagePersistence senden fehlgeschlagen bzw. keine Antwort erhalten
            //reschedulen?
            response = new ParentConnectionResponse();
            response.setRang(new BigInteger(Integer.toString(HttpTransportConstants.STATUS_BAD_REQUEST)));
            logger.error("Could not register with parent server");
            logger.error(wse.toString());
            wse.printStackTrace();
        }
        return response;
    }



    private ParentConnectionResponse sendRequest(ParentConnectionRequest request) {
        ParentConnectionResponse response = (ParentConnectionResponse) marshalSendAndReceive(request.getParentUrl(), request);
        return response;
    }
}
