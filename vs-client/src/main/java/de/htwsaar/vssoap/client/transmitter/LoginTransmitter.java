package de.htwsaar.vssoap.client.transmitter;

import de.htwsaar.vssoap.client.Session;
import de.htwsaar.vssoap.client.exception.LoginException;
import de.htwsaar.vssoap.client.view.MainController;
import de.htwsaar.vssoap.domain.GetLoginRequest;
import de.htwsaar.vssoap.domain.GetLoginResponse;
import de.htwsaar.vssoap.domain.GetLogoutRequest;
import de.htwsaar.vssoap.domain.GetLogoutResponse;
import de.htwsaar.vssoap.parent.AbstractTransmitter;
import de.htwsaar.vssoap.parent.Roles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.WebServiceIOException;

/**
 * @author cedosw
 */
@Component
public class LoginTransmitter extends AbstractTransmitter {
    private final Logger log = LoggerFactory.getLogger(LoginTransmitter.class);

    @Autowired
    private Session session;
    @Autowired
    MainController mainController;

    public boolean login(String username, String password) throws LoginException {
        try {
            GetLoginRequest request = new GetLoginRequest();
            request.setUsername(username);
            request.setPassword(password);
            GetLoginResponse response = (GetLoginResponse) marshalSendAndReceive(request);
            if (response.getRole().equalsIgnoreCase(Roles.ADMIN.toString()) || response.getRole().equalsIgnoreCase(Roles.USER.toString())) {
                session.setUsername(username);
                session.setRole(response.getRole());
                session.setDepartment(response.getDepartment());
                return true;
            }

            return false;
        } catch (Exception e) {
            throw new LoginException("Client: Error on logging in.", e);
        }
    }

    public boolean logout(boolean logout) throws WebServiceIOException{
        GetLogoutRequest request = new GetLogoutRequest();
        request.setLogout(logout);
        request.setUri(getConnection().getUrl());
        GetLogoutResponse response = (GetLogoutResponse) marshalSendAndReceive(request);
        if(response.isLogout() == true){
            getConnection().setCredentials("", "");
            return true;
        }
            return false;
    }
}
