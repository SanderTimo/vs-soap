package de.htwsaar.vssoap.client.endpoint;

import de.htwsaar.vssoap.client.exception.MessageException;
import de.htwsaar.vssoap.client.view.MainController;
import de.htwsaar.vssoap.client.view.MessageList;
import de.htwsaar.vssoap.domain.CreateMessageRequest;
import de.htwsaar.vssoap.domain.CreateMessageResponse;
import de.htwsaar.vssoap.domain.Message;
import javafx.application.Platform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 * Created by Timo on 24.01.2017.
 */
@Endpoint
public class ClientMessageEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(ClientMessageEndpoint.class);
    private static final String NAMESPACE_URI = "http://htwsaar.de/vssoap/domain";

    @Autowired
    private MessageList messageList;

    @Autowired
    private MainController mainController;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "createMessageRequest")
    @ResponsePayload
    public CreateMessageResponse getMessage(@RequestPayload CreateMessageRequest request) throws MessageException {
        try {
            CreateMessageResponse response = new CreateMessageResponse();
            response.setMessage(request.getMessage());
            logger.info("Message received: " + request.getMessage().getGuid());
            Message message = response.getMessage();
            message.setProgress("Done");
            if(request.getMessage().getActionType() == null || request.getMessage().getActionType().equals("SAVE") ) {
                //einbinden in JavaFx Thread:
                Platform.runLater(new Runnable() {
                                      @Override
                                      public void run() {
                                          mainController.updateOwnerList(message);
                                          messageList.addMessage(message);
                                          mainController.updateDepartmentList(message);
                                      }
                                  });

            }else{
                messageList.deleteMessage(message);
            }
            return response;
        } catch(Exception e) {
            throw new MessageException("Error recieving message on client.", e);
        }
    }
}
