package de.htwsaar.vssoap.client.transmitter;

import de.htwsaar.vssoap.domain.GetCreateUserRequest;
import de.htwsaar.vssoap.domain.GetCreateUserResponse;
import de.htwsaar.vssoap.parent.AbstractTransmitter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.WebServiceFaultException;
import org.springframework.ws.client.WebServiceIOException;

/**
 * @author cedosw (27.03.2017)
 */
@Component
public class UserTransmitter extends AbstractTransmitter {
    private final Logger log = LoggerFactory.getLogger(UserTransmitter.class);

    public GetCreateUserResponse createUser(String username, String password, String role) throws WebServiceIOException, WebServiceFaultException {
        GetCreateUserRequest request = new GetCreateUserRequest();
        request.setUsername(username);
        request.setPassword(password);
        request.setRole(role);
        GetCreateUserResponse response = (GetCreateUserResponse) marshalSendAndReceive(request);
        return response;
    }
}
