package de.htwsaar.vssoap.client.view;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import de.htwsaar.vssoap.client.Router;
import de.htwsaar.vssoap.client.Session;
import de.htwsaar.vssoap.client.transmitter.ParentSetupTriggerTransmitter;
import de.htwsaar.vssoap.domain.ParentSetupTriggerResponse;
import de.htwsaar.vssoap.parent.ConnectionConfig;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import org.bouncycastle.asn1.crmf.ProofOfPossession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.WebServiceTransportException;
import org.springframework.ws.soap.SoapFaultException;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Dialog zum Einrichten einer Verbindung vom eigenen Server zu einem Parent
 * Nur für Superuser zugänglich
 *
 * Created by Julian on 17.03.2017.
 */
public class SetParentController implements Initializable {

    private static final Logger logger = LoggerFactory.getLogger(SetParentController.class);


    @Autowired
    private Router router;

    @Autowired
    private ConnectionConfig connection;

    @Autowired
    private ParentSetupTriggerTransmitter transmitter;

    @Autowired
    private Session session;

    @FXML
    private JFXTextField setParentInput;

    @FXML
    private JFXButton exitButton;

    @FXML
    private JFXButton connectButton;

    @FXML
    private Label departmentLabel;

    @FXML
    private Label hostLabel;


    public void initialize(URL url, ResourceBundle bundle) {
        departmentLabel.setText(session.getDepartment());
        hostLabel.setText(connection.getParentUrl());
    }

    @FXML
    protected void onClickConnect() {
        this.connectButton.disableProperty().setValue(true);

        Runnable runnable = () -> {
            try {
                connectParent();
                ToastView.makeMessage(router.getStage(), "Connection Request sent.", 2000, 250, 250);
                exitActionless();
            } catch (Exception e) {
                logger.error(e.toString());
                ToastView.makeErrorMessage(router.getStage(), "Cannot connect to server! Pls verify Host or Connection.", 2000, 250, 250);
                exitActionless();
            }
        };
        runnable.run();
    }

    private boolean connectParent() throws SoapFaultException, WebServiceIOException {
        String targetAdress = setParentInput.getText();
        logger.info("Sende Connection Anfrage von: " + connection.getParentUrl() + " an: " + targetAdress);
        if (targetAdress == null || targetAdress.isEmpty()) {
            ToastView.makeErrorMessage(router.getStage(), "Address may not be empty", 2000, 250, 250);
            return false;
        }

        targetAdress = makeUrl(targetAdress);
        transmitter.setDefaultUri(connection.getParentUrl());

        ParentSetupTriggerResponse response = transmitter.sendParentSetupTrigger(targetAdress);
        logger.info("Connection Anfrage Antwort: " + response.getStatus());

        if (HttpStatus.BAD_REQUEST.toString().equals(response.getStatus())) {
            throw new WebServiceTransportException("Connection Fehler. Ungültige Anfragemessage");
        }
        return true;
    }

    private String makeUrl(String input) {
        String url = input.trim();
        if (!url.endsWith(connection.getLocation())) {
            if (url.endsWith("/")) {
                url = url.substring(0, url.length() - 2);
            }
            url = url.concat(connection.getLocation());
        }
        if (!url.startsWith("http://")) {
            url = "http://".concat(url);
        }
        return url;
    }

    @FXML
    protected void onClickExit(ActionEvent event) {
        router.closeModal((Parent) ((JFXButton) event.getTarget()).getParent());
        event.consume();
    }

    /**
     * Sauberes Schließen des Dialogs mithilfe des Routers
     */
    private void exitActionless() {
        exitButton.fireEvent(new ActionEvent());
    }
}
