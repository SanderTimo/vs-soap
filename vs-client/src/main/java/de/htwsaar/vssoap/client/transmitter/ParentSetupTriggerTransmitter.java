package de.htwsaar.vssoap.client.transmitter;

import de.htwsaar.vssoap.domain.ParentSetupTriggerRequest;
import de.htwsaar.vssoap.domain.ParentSetupTriggerResponse;
import de.htwsaar.vssoap.parent.AbstractTransmitter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.WebServiceIOException;
import java.math.BigInteger;

/**
 * Transmitter für das Übertragen einer SetParent Message
 *
 * Sendet TriggerRequest an eigenen parent
 * dort wird ausgelöst, dass sich der server bei dem angegeben parent registriert
 * erhält vom eigenen parent bestätigung, dass der request eingegangen ist
 *
 * @author JulianW
 */
@Component
public class ParentSetupTriggerTransmitter extends AbstractTransmitter {
    private static final Logger logger = LoggerFactory.getLogger(ParentSetupTriggerTransmitter.class);

    public ParentSetupTriggerResponse sendParentSetupTrigger(String parentUrl) throws WebServiceIOException {
        ParentSetupTriggerRequest request = new ParentSetupTriggerRequest();
        request.setParentUrl(parentUrl);
        ParentSetupTriggerResponse response;

        //for error case
        response = new ParentSetupTriggerResponse();
        response.setStatus(HttpStatus.NOT_FOUND.toString());

        response = sendRequest(request);
        logger.info("The Server has acknowledged the ParentSetupTriggerRequest");
        return response;
    }

    private ParentSetupTriggerResponse sendRequest(ParentSetupTriggerRequest request) {
        ParentSetupTriggerResponse response = (ParentSetupTriggerResponse) marshalSendAndReceive(request);
        return response;
    }
}
