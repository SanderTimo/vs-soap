package de.htwsaar.vssoap.client.exception;

/**
 * @author cedosw (28.03.2017)
 */
public class LoginException extends Exception {
    public LoginException(String msg) {
        super(msg);
    }

    public LoginException(String msg, Exception cause) {
        super(msg, cause);
    }
}
