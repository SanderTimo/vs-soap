/*package de.htwsaar.vssoap.client.view;

import de.htwsaar.vssoap.domain.CreateMessageResponse;
import de.htwsaar.vssoap.domain.Message;
import de.htwsaar.vssoap.parent.ConnectionConfig;
import de.htwsaar.vssoap.parent.MessageClient;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Oguzhan
 */
//@Component
/*
public class SendMessageTaskView  {

    private static Logger logger = LoggerFactory.getLogger(SendMessageTaskView.class);

    private ConnectionConfig connection;

    private MessageClient transmitter;

    private boolean serverConnected = false;
    private BooleanProperty connectionState_booleanProperty = new SimpleBooleanProperty(false);

    private TableColumn subject_TC = new TableColumn("Subject");
    private TableColumn message_TC = new TableColumn("Message");

    private VBox vBox;
    private Scene scene;
    private TextArea textArea;
    private ObservableList<Message> messageList = FXCollections.observableArrayList();
    private Label label;
    private Button connect;
    private Button disconnect;

    private final String disconnectd = "Nachricht konnte nicht gesendet werden, \n" +
            "sobald eine Verbindung zum Server wiederhergestellt ist,\n" +
            "wird Ihre Nachricht automatisch weitergeleitet\n" +
            "! Beim Beenden des Client Anwendungs gehen nicht versendete Nachrichten verloren !";

    private final String connected = "Mit Server Verbunden";

    private TableView table = new TableView();

    public SendMessageTaskView(ConnectionConfig connection, MessageClient transmitter) {
        this.connection = connection;
        this.transmitter = transmitter;
        init();
        queueScene();
    }

    public void init() {
        vBox = new VBox();
        scene = new Scene(vBox, 400, 100);
        label = new Label("QueueView");
        textArea = new TextArea(disconnectd);
        textArea.setPrefHeight(50);

        // nur zum Testen der Queue bei Verbindungsproblemen
        connect = new Button("Connect");
        disconnect = new Button("Disconnect");

        subject_TC = new TableColumn("Subject");
        message_TC = new TableColumn("Message");

        table.getColumns().addAll(subject_TC, message_TC);

        table.setItems(messageList);

        subject_TC.setCellValueFactory(new PropertyValueFactory<Message, String>("subject"));
        message_TC.setCellValueFactory(new PropertyValueFactory<Message, String>("messageText"));
    }

    private void queueScene() {
        Stage stage = new Stage();

        connectionState_booleanProperty.addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    textArea.setText(connected);
                   // sendMessage();
                } else {
                    textArea.setText(disconnectd);
                    //task();
                }
            }
        });

        connect.setOnAction(e -> {
            connectionState_booleanProperty.set(true);
        });
        disconnect.setOnAction(e -> {
            connectionState_booleanProperty.set(false);
        });

        textArea.setEditable(false);

        vBox.getChildren().addAll(label, textArea, table, connect, disconnect);
        stage.setTitle("QueueView ");
        stage.setScene(scene);

        stage.show();
    }

    public void setConnectionState(boolean connected) {
        connectionState_booleanProperty.setValue(connected);
    }

    public void addMessageToTask(Message message) {
        logger.info("Neue Message zum Absenden aufgenommen.");
        messageList.add(message);
        sendMessage();
    }

    private void sendMessage() {
        if (messageList.isEmpty()) return;

        while (!messageList.isEmpty()) {

            // Wenn Senden erfolgreich, dann lösche die Nachricht aus der Liste
            if (sendingTask(messageList.get(0))) {
                messageList.remove(0);
            }
            // Ansonnsten versuche die Nachricht nach 20 Sekunden wieder zu Versenden
            else {
                connectionState_booleanProperty.set(false);
//                task();
                logger.error("Nachricht konnte nicht gesendet werden!\n" +
                        "Sendevorgang wird in 20 Sekunden wiederholt versucht");
                return;
            }
        }
    }

    private void task() {
        if (messageList.isEmpty()) return;

        Thread th = new Thread(
                new Task() {
                    @Override
                    protected Object call() throws Exception {

                        while (!messageList.isEmpty()) {

                            if (sendingTask(messageList.get(0))) {
                                connectionState_booleanProperty.set(true);
                                messageList.remove(0);
                                updateMessage("Thread Cancelled" + " Reconnected to Server ");
                                cancel();
                            }

                            if (messageList.isEmpty()) {
                                cancel();
                            }

                            if (connectionState_booleanProperty.getValue()) {
                                updateMessage("Thread Cancelled" + " Reconnectet with Server");
                                cancel();
                            }

                            if (isCancelled()) {
                                updateMessage("Thread Cancelled: " + " Cancelled ");
                                break;
                            }

                            // Now block the thread for 5 sec and check if canceled
                            try {
                                Thread.sleep(200000);
                            } catch (InterruptedException interrupted) {
                                if (isCancelled()) {
                                    updateMessage("Thread closed");
                                    break;
                                }
                            }
                        }
                        return true;
                    }
                });
        th.setDaemon(true);
        th.start();
    }

    public VBox getSendMessageTaskView_VBox() {
        return vBox;
    }

    public boolean sendingTask(Message message) {
        CreateMessageResponse response;
        if (message == null) {
            logger.info("message null");
        } else {
            logger.info(message.toString());
        }
        message.setSourceUri(connection.getUrl());
        connectionState_booleanProperty.setValue(true);
        messageList.clear();
        response = (CreateMessageResponse) transmitter.sendMessageToUri(message, connection.getParentUrl());

        // TODO alterntive prüfung auf fehler

        if (!connectionState_booleanProperty.getValue() || response.getMessage().getMessageText().contains("Beim Senden der Message ist ein Fehler aufgetreten")) {
            // TODO msg in eine queue speichern
            logger.info("message fehler");
            logger.info("t2" + !connectionState_booleanProperty.getValue());
            setConnectionState(false);
            return false;
        }
        logger.info("Message erfolgreich gesendet");
        return true;
    }

//    public void sendController() {
//        init();
//        queueScene();
//    }
//
//    @Override
//    public void run() {
//        sendController();
//    }

}
        */