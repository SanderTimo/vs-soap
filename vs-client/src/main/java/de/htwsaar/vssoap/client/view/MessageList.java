package de.htwsaar.vssoap.client.view;

import de.htwsaar.vssoap.domain.Message;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.validator.internal.util.logging.Log_$logger;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Hauptmessageliste kann bereits bevor die GUI
 * fertig geladen ist von externen Anfragen befüllt werden
 *
 * Created by Julian on 26.02.2017.
 */
@Component
public class MessageList {

    private ObservableList<Message> messageList = FXCollections.synchronizedObservableList(FXCollections.observableArrayList());

    public ObservableList<Message> getMessageList(){
        return this.messageList;
    }

    /**
     * Fügt eine nachricht in die messageList ein
     * überprüft mittels Guid ob die Nachricht schon vorhanden ist und ersetzt diese
     * @param message
     */
    public void addMessage(Message message){
        ArrayList<Message> delMessages = new ArrayList<Message>();
        for (Message m : messageList) {
            if(m.getGuid().equals(message.getGuid())){
                delMessages.add(m);
            }
        }
        messageList.removeAll(delMessages);
        this.messageList.add(message);
    }

    /**
     * Löscht eine nachricht aus dem der messageList
     * @param message
     */
    public void deleteMessage(Message message){
        for (int i = 0; i < messageList.size(); i++) {
            if(messageList.get(i).getGuid().equals(message.getGuid())){
                messageList.remove(i);
            }
        }
    }
}
