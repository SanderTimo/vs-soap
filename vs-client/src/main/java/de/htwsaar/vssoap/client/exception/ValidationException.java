package de.htwsaar.vssoap.client.exception;

/**
 * @author cedosw (22.03.2017)
 */
public class ValidationException extends Exception {
    public ValidationException(String msg) {
        super(msg);
    }
}
