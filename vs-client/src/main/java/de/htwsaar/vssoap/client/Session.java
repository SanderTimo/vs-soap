package de.htwsaar.vssoap.client;

import de.htwsaar.vssoap.parent.Roles;
import org.springframework.stereotype.Component;

/**
 * @author cedosw (22.03.2017)
 */
@Component
public class Session {
    private String username;
    private String password;
    private String role;
    private String department;

    public String getDepartment() {return department;}

    public void setDepartment(String department) {this.department = department;}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isAdmin(){
        return Roles.ADMIN.toString().equals(role);
    }
}
