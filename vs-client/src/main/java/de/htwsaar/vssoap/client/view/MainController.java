package de.htwsaar.vssoap.client.view;



import de.htwsaar.vssoap.client.Router;
import de.htwsaar.vssoap.client.messaging.SendMessageTask;
import de.htwsaar.vssoap.domain.Message;
import de.htwsaar.vssoap.parent.*;
import de.htwsaar.vssoap.client.transmitter.LoginTransmitter;
import de.htwsaar.vssoap.client.Session;
import de.htwsaar.vssoap.parent.ActionType;
import de.htwsaar.vssoap.parent.ConnectionConfig;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.client.WebServiceIOException;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.net.URL;
import java.util.*;

public class MainController implements Initializable {

    @Autowired
    private Router router;

    @Autowired
    SendMessageTask sendMessageTask;

    @Autowired
    ConnectionConfig connectionConfig;

    @Autowired
    LoginTransmitter loginClient;

    @Autowired
    private Session session;

    @FXML
    ListView<String> listView_Names;
    @FXML
    ListView<String> listView_Departments;
    @FXML
    ListView<String> listView_Progress;
    @FXML
    TableView<Message> tableView_Table;
    @FXML
    TableColumn<Message, String> tableColumn_Message;
    @FXML
    TableColumn<Message, String> tableColumn_Owner;
    @FXML
    TableColumn<Message, String> tableColumn_Department;
    @FXML
    TableColumn<Message, XMLGregorianCalendar> tableColumn_Date;
    @FXML
    TableColumn<Message, Boolean> tableColumn_Progress;
    @FXML
    TableColumn<Message, String> tableColumn_lastEdited;
    @FXML
    TextField filterField;
    @FXML
    TextField textField_Subject;
    @FXML
    TextArea textArea_Message;
    @FXML
    TextField searchField;
    @FXML
    Button searchButton;
    @FXML
    Button msgButton;
    @FXML
    Button filterButton;
    @FXML
    Button removeFilterButton;
    @FXML
    Button editButton;
    @FXML
    Button deleteButton;
    @FXML
    Button saveButton;
    @FXML
    Button approvalButton;
    @FXML
    Button pushButton;
    @FXML
    Button addUserButton;
    @FXML
    Button configButton;
    @FXML
    TextField userNameField;
    @FXML
    Label pushLabel;
    @FXML
    Label newUserLabel;
    @FXML
    Label addUserLabel;
    @FXML
    Label configLabel;
    @FXML
    Label pendingLabel;

    @Autowired
    MessageList messageListExternal;

    private static final String APPROVAL = "Needs Approval";
    private static final String DONE = "Done";



    private ObservableList<Message> messageList =  FXCollections.synchronizedObservableList(FXCollections.observableArrayList());

    private ObservableList<Message> filteredList;
    private ObservableList<Message> searchList;


    public Message message;

    public MainController(){}



    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tableView_Table.setPlaceholder(new Label("No content in the table"));
        this.messageList=messageListExternal.getMessageList();
        tableView_Table.setItems(messageList);
        addDepartmentFromLogin(session.getDepartment());

        if(session.getRole().equals(Roles.ADMIN.toString())){
            activateAdminButtons();
        }

        userNameField.setText("Logged in as: " + session.getUsername() );

        DropShadow shadow = new DropShadow();

        //  create lists

        filteredList = FXCollections.observableArrayList();
        searchList = FXCollections.observableArrayList();




        // tableview
        tableColumn_Message.setCellValueFactory(new PropertyValueFactory<Message, String>("subject"));
        tableColumn_Owner.setCellValueFactory(new PropertyValueFactory<Message, String>("owner"));
        tableColumn_Department.setCellValueFactory(new PropertyValueFactory<Message, String>("department"));
        tableColumn_Date.setCellValueFactory(new PropertyValueFactory<Message, XMLGregorianCalendar>("updatedAt"));
        tableColumn_Progress.setCellValueFactory(new PropertyValueFactory<Message, Boolean>("approvalNeeded"));
        tableColumn_Progress.setCellFactory(tc -> new TableCell<Message, Boolean>() {
            @Override
            protected void updateItem(Boolean item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? null :
                        item.booleanValue() ? APPROVAL : DONE);
            }
        });
        tableColumn_lastEdited.setCellValueFactory(new PropertyValueFactory<Message, String>("updatedBy"));


        fillListViews();            // load dummy data in tableview
        loadStyles();                // load button styles

        //delete Button nur als Admin anklickbar
        if(!session.isAdmin()){
            deleteButton.setDisable(true);
        }

        // when delete button clicked, delete messages from tableview
        deleteButton.setOnAction(e -> {
            Message selectedItem = tableView_Table.getSelectionModel().getSelectedItem();
            selectedItem.setActionType(ActionType.DELETE.toString());
            sendMessageTask.sendMessage(selectedItem);
        });
        // safe changes in messages and tableview when button is clicked
        saveButton.setOnAction(e -> {
            Message message;
            message = tableView_Table.getSelectionModel().getSelectedItem();
            message.setSubject(textField_Subject.getText());
            message.setMessageText(textArea_Message.getText());
            message.setUpdatedBy(session.getUsername());
            message.setActionType(ActionType.SAVE.toString());
            try {
                GregorianCalendar c = new GregorianCalendar();
                c.setTime(new Date());
                message.setUpdatedAt(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
            }catch (DatatypeConfigurationException dataTypeException){
                //message date cannot be updated for unknown reason
            }

            textArea_Message.setEditable(false);
            if(message.getRang()<connectionConfig.getRang()){
                if(!session.isAdmin()){
                    message.setApprovalNeeded(true);
                }
            } else {
                message.setApprovalNeeded(false);
            }
            tableView_Table.getSelectionModel().clearSelection();

            sendMessageTask.sendMessage(message);

            tableView_Table.getColumns().get(0).setVisible(false);
            tableView_Table.getColumns().get(0).setVisible(true);
        });

        approvalButton.setOnAction(e -> {
            Message message;
            message = tableView_Table.getSelectionModel().getSelectedItem();
            message.setSubject(textField_Subject.getText());
            message.setMessageText(textArea_Message.getText());
            message.setUpdatedBy(session.getUsername());
            textArea_Message.setEditable(false);
            message.setApprovalNeeded(false);

            message.setActionType(ActionType.SAVE.toString());
            try {
                GregorianCalendar c = new GregorianCalendar();
                c.setTime(new Date());
                message.setUpdatedAt(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
            }catch (DatatypeConfigurationException dataTypeException){
                //message date cannot be updated for unknown reason
            }

            sendMessageTask.sendMessage(message);
            tableView_Table.getSelectionModel().clearSelection();

            tableView_Table.getColumns().get(0).setVisible(false);
            tableView_Table.getColumns().get(0).setVisible(true);
        });


        // show filtered tableview when button is clicked
        filterButton.setOnAction(e -> {

            String name = listView_Names.getSelectionModel().getSelectedItem();
            String department = listView_Departments.getSelectionModel().getSelectedItem();
            String progress = listView_Progress.getSelectionModel().getSelectedItem();
            filterField.setText("Filter selected: " + name + " & " + department + " & " + progress);
            filteredList.clear();

            for (int i = 0; i < messageList.size(); i++) {
                textArea_Message.clear();
                textField_Subject.clear();
                message = messageList.get(i);
                if (listView_Names.getSelectionModel().getSelectedItem() == null && listView_Departments.getSelectionModel().getSelectedItem() == null && listView_Progress.getSelectionModel().getSelectedItem() == null) {
                    // errormessage
                    filterButton.setDisable(true);
                    ToastView.makeErrorMessage(router.getStage(), "No filteroption selected!", 2500, 250, 250);
                    Timer timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            filterButton.setDisable(false);
                        }
                    }, 4000);

                    //namen, kein department ,kein progress
                } else if (listView_Names.getSelectionModel().getSelectedItem() != null && listView_Departments.getSelectionModel().getSelectedItem() == null && listView_Progress.getSelectionModel().getSelectedItem() == null) {
                    if (message.getOwner().equals(name)) {
                        filteredList.add(message);
                        tableView_Table.setItems(filteredList);
                        tableView_Table.getColumns().get(0).setVisible(false);
                        tableView_Table.getColumns().get(0).setVisible(true);

                    }else{
                        tableView_Table.setItems(filteredList);
                    }
                    //kein name , department,kein progress
                } else if (listView_Names.getSelectionModel().getSelectedItem() == null && listView_Departments.getSelectionModel().getSelectedItem() != null && listView_Progress.getSelectionModel().getSelectedItem() == null) {
                    if (message.getDepartment().equals(department)) {
                        filteredList.add(message);
                        tableView_Table.setItems(filteredList);
                        tableView_Table.getColumns().get(0).setVisible(false);
                        tableView_Table.getColumns().get(0).setVisible(true);

                    }else{
                        tableView_Table.setItems(filteredList);
                    }
                    //kein name,kein department,progress
                } else if (listView_Names.getSelectionModel().getSelectedItem() == null && listView_Departments.getSelectionModel().getSelectedItem() == null && listView_Progress.getSelectionModel().getSelectedItem() !=null) {
                    if (mapProgress(message).equals(progress)) {
                        filteredList.add(message);
                        tableView_Table.setItems(filteredList);
                        tableView_Table.getColumns().get(0).setVisible(false);
                        tableView_Table.getColumns().get(0).setVisible(true);

                    }else{
                        tableView_Table.setItems(filteredList);
                    }
                   //name,department,kein progress
                } else if (listView_Names.getSelectionModel().getSelectedItem() != null && listView_Departments.getSelectionModel().getSelectedItem() != null && listView_Progress.getSelectionModel().getSelectedItem() == null) {
                    if (message.getOwner().equals(name) && message.getDepartment().equals(department)) {
                        filteredList.add(message);
                        tableView_Table.setItems(filteredList);
                        tableView_Table.getColumns().get(0).setVisible(false);
                        tableView_Table.getColumns().get(0).setVisible(true);

                    }else{
                        tableView_Table.setItems(filteredList);
                    }
                   //  name,kein department,progress
                } else if (listView_Names.getSelectionModel().getSelectedItem() != null && listView_Departments.getSelectionModel().getSelectedItem() == null && listView_Progress.getSelectionModel().getSelectedItem() != null) {
                    if (message.getOwner().equals(name) && mapProgress(message).equals(progress)) {
                        filteredList.add(message);
                        tableView_Table.setItems(filteredList);
                        tableView_Table.getColumns().get(0).setVisible(false);
                        tableView_Table.getColumns().get(0).setVisible(true);
                    }else{
                        tableView_Table.setItems(filteredList);
                    }
                      //kein name,department,progress
                } else if (listView_Names.getSelectionModel().getSelectedItem() == null && listView_Departments.getSelectionModel().getSelectedItem() != null && listView_Progress.getSelectionModel().getSelectedItem() != null) {
                    if (message.getDepartment().equals(department) && mapProgress(message).equals(progress)) {
                        filteredList.add(message);
                        tableView_Table.setItems(filteredList);
                        tableView_Table.getColumns().get(0).setVisible(false);
                        tableView_Table.getColumns().get(0).setVisible(true);
                    }else{
                        tableView_Table.setItems(filteredList);
                    }
                 //name,department,progress
                } else if (listView_Names.getSelectionModel().getSelectedItem() != null && listView_Departments.getSelectionModel().getSelectedItem() != null && listView_Progress.getSelectionModel().getSelectedItem() != null) {
                    if (message.getOwner().equals(name) && message.getDepartment().equals(department) && mapProgress(message).equals(progress)) {
                        filteredList.add(message);
                        tableView_Table.setItems(filteredList);
                        tableView_Table.getColumns().get(0).setVisible(false);
                        tableView_Table.getColumns().get(0).setVisible(true);
                    }else{
                        tableView_Table.setItems(filteredList);
                    }

                }


            }

        });


        //Style Geschichten:

        filterButton.addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        filterButton.setEffect(shadow);
                    }
                });

        filterButton.addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        filterButton.setEffect(null);
                    }
                });

        removeFilterButton.addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        removeFilterButton.setEffect(shadow);
                    }
                });

        removeFilterButton.addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        removeFilterButton.setEffect(null);
                    }
                });
        msgButton.addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        msgButton.setEffect(shadow);
                    }
                });

        msgButton.addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        msgButton.setEffect(null);
                    }
                });
        pushButton.addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        pushButton.setEffect(shadow);
                    }
                });

        pushButton.addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        pushButton.setEffect(null);
                    }
                });
        addUserButton.addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        addUserButton.setEffect(shadow);
                    }
                });

        addUserButton.addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        addUserButton.setEffect(null);
                    }
                });
        configButton.addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        configButton.setEffect(shadow);
                    }
                });

        configButton.addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        configButton.setEffect(null);
                    }
                });
        searchButton.addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        searchButton.setEffect(shadow);
                    }
                });

        searchButton.addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        searchButton.setEffect(null);
                    }
                });



        // load complete messageList, remove selected filter options
        removeFilterButton.setOnAction(e -> {
            listView_Names.getSelectionModel().clearSelection();
            listView_Progress.getSelectionModel().clearSelection();
            listView_Departments.getSelectionModel().clearSelection();
            filteredList.clear();
            searchList.clear();
            tableView_Table.setItems(messageList);
            tableView_Table.getColumns().get(0).setVisible(false);
            tableView_Table.getColumns().get(0).setVisible(true);
            filterField.setText("No filter selected");



        });
        // search for strings in messages, upper and lowercases ignored
        searchButton.setOnAction(e -> {

            if(searchField.getText().toLowerCase().isEmpty())
            {
                ToastView.makeErrorMessage(router.getStage(), "No search term found! Please type in a search term", 2500, 250, 250);
            }
            else {
            textArea_Message.clear();
            textField_Subject.clear();
            String search = searchField.getText().toLowerCase();
            searchList.clear();
            tableView_Table.setItems(searchList);
            for (int i = 0; i < messageList.size(); i++) {

                message = messageList.get(i);
                String owner = message.getOwner().toLowerCase();
        // TODO  Sobald Departments in Messages mitgesendet werden kann der Part wieder einkommentiert werden
                // String department = message.getDepartment().toLowerCase();
                String department = "";           // Dient dazu momentan den Nullpointer zu vermeiden
                String progress = mapProgress(message);
                String subject = message.getSubject().toLowerCase();
                String date = message.getUpdatedAt().toGregorianCalendar().getTime().toString();
                String lastedited = message.getUpdatedBy().toLowerCase();
                String content = message.getMessageText().toLowerCase();

                //if(message.getOwner() == search || message.getDepartment() == search || message.getProgress() == search || message.getMessageSubject() == search || message.getLastEditedBy() == search || message.getDate() == search)
                if (owner.contains(search) || department.contains(search) || progress.contains(search) || subject.contains(search) || date.contains(search) || lastedited.contains(search) || content.contains(search)) {
                    searchList.add(message);
                    tableView_Table.setItems(searchList);
                    tableView_Table.getColumns().get(0).setVisible(false);
                    tableView_Table.getColumns().get(0).setVisible(true);
                }
            }

        }});
        pushButton.setOnAction( ev -> {
            if (tableView_Table.getSelectionModel().getSelectedItem() == null)
            {
                ToastView.makeErrorMessage(router.getStage(), "No message selected. Please select a message!", 2500, 250, 250);
            }
            Message message;
            message = tableView_Table.getSelectionModel().getSelectedItem();
            if(message.getRang()==0){
                ToastView.makeErrorMessage(router.getStage(), "Nachricht kann nicht gepushed werden, kein Parent vorhanden", 2500, 250, 250);
                return;
            }
            if(message.getRang()!=connectionConfig.getRang()) {
                ToastView.makeErrorMessage(router.getStage(), "Nachricht kann nur vom Erstellenden Department gepushed werden.", 2500, 250, 250);
                return;
            }
            message.setSubject(textField_Subject.getText());
            message.setMessageText(textArea_Message.getText());
            message.setUpdatedBy(session.getUsername());
            message.setActionType(ActionType.SAVE.toString());
            message.setType(PermissionType.PUBLIC.toString());
            message.setApprovalNeeded(false);

            message.setRang(message.getRang()-1);
            //setze update datum
            try {
                GregorianCalendar c = new GregorianCalendar();
                c.setTime(new Date());
                message.setUpdatedAt(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
            }catch (DatatypeConfigurationException dataTypeException){
                //message date cannot be updated for unknown reason
            }
            textArea_Message.setEditable(false);
            sendMessageTask.sendMessage(message);
            tableView_Table.getSelectionModel().clearSelection();
            tableView_Table.getColumns().get(0).setVisible(false);
            tableView_Table.getColumns().get(0).setVisible(true);

        });

        configButton.setOnAction( ev ->{
            router.showModal("/setparent.fxml", "/Theme.css", 450, 250,"Parent setzen");
        });


        tableView_Table.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue==null){
                editButton.setDisable(true);
                deleteButton.setDisable(true);
                saveButton.setDisable(true);
                approvalButton.setDisable(true);
                pushButton.setDisable(true);
            } else {
                Message message = tableView_Table.getSelectionModel().getSelectedItem();
                showMessageContent(message);
                textArea_Message.setEditable(false);
                editButton.setDisable(editForbidden(message));
                saveButton.setDisable(editForbidden(message));
                deleteButton.setDisable(deleteForbidden(message));
                pushButton.setDisable(pushForbidden(message));
                toggleApprovalButton(message);
            }
        });

    }

    public void updatePendingMessage(int pendingcounter){
        if(pendingcounter<=0){
            pendingLabel.setText("");
        }
        else{
            pendingLabel.setText("Messages pending: "+ pendingcounter);
        }
    }


    private void activateAdminButtons(){
        if(session.isAdmin()){
            pushButton.setVisible(true);
            pushLabel.setVisible(true);
            configButton.setVisible(true);
            configLabel.setVisible(true);
            //wird erst bei wahl einer message gezeigt
            approvalButton.setVisible(false);
        }
        saveButton.setDisable(true);
        approvalButton.setDisable(true);
    }


    private void toggleApprovalButton(Message message){
        if(!message.isApprovalNeeded()){
            approvalButton.setDisable(true);
            return;
        }

        if(session.isAdmin()){
            approvalButton.setVisible(true);
            approvalButton.setDisable(false);
        } else {
            approvalButton.setVisible(false);
            approvalButton.setDisable(true);
        }
    }

    private boolean pushForbidden(Message message){
        if(Roles.ADMIN.toString().equals(session.getRole())) {
            //ist admin

            if(message.getRang()<connectionConfig.getRang()){
                //nachricht ist von oben
                //nicht pushen
                return true;
            }
            //ok:
            return false;
        }
        //not allowed to push:
        return true;
    }

    private boolean editForbidden(Message message){
        if(Roles.ADMIN.toString().equals(session.getRole())) {
            //admin
            return false;
        }
        if(!session.getUsername().equals(message.getOwner())){
            //anderer user, kein admin
            return true;
        }
        if(message.getRang()<connectionConfig.getRang() && message.isApprovalNeeded()==false){
            //weitergeleitet nach oben und bereits vom admin bestätigt:
            //nichtmehr vom owner änderbar
            return true;
        }
        //owner && not yet approved
        return false;
    }

    private boolean deleteForbidden (Message message) {
        return message.getRang() < connectionConfig.getRang() || !session.isAdmin();
    }

    // fill listView with dumnmy data
    public void fillListViews() {
        ObservableList<String> progress = FXCollections.observableArrayList(APPROVAL, DONE);

        listView_Progress.setItems(progress);
    }

    public void fillTable(ObservableList<Message> messageList) {

    }

    /**
     * Überprüft ob der Besitzer schon in der OwnerList steht
     * falls nicht fügt er diesen dort ein
     * @param message
     */
    public void updateOwnerList(Message message){
        if(message==null || message.getOwner()==null || message.getOwner().isEmpty())
            return;
        ObservableList<String> names = FXCollections.synchronizedObservableList(FXCollections.observableArrayList());
        if(listView_Names!=null && listView_Names.getItems() != null ){
            names.setAll(listView_Names.getItems());
        }
        boolean isInList = false;
        for(int i= 0;i< names.size();i++){
            if(names.get(i).equals(message.getOwner())){
                isInList = true;
            }
        }
        if(!isInList){
            names.add(message.getOwner());
        }

        listView_Names.setItems(names);
    }

    /**
     * Überprüft ob des Department schon in der DepartmentList steht
     * falls nicht fügt er dieses dort ein
     * @param message
     */
    public void updateDepartmentList(Message message){
        if(message==null || message.getDepartment()==null || message.getDepartment().isEmpty())
            return;
        ObservableList<String> department = FXCollections.synchronizedObservableList(FXCollections.observableArrayList());
        if(listView_Departments!=null && listView_Departments.getItems() != null ){
          department.setAll(listView_Departments.getItems());
        }
        boolean isInList = false;
        for(int i= 0;i< department.size();i++){
            if(department.get(i).equals(message.getDepartment())){
                isInList = true;
            }
        }
        if(!isInList){
            department.add(message.getDepartment());
        }
        listView_Departments.setItems(department);
    }

    /**
     * Fügt die Message in den Progress done oder approval needed ein
     * @param message
     */
    public String mapProgress(Message message){
        if(message==null) return "";
        return message.isApprovalNeeded() ? APPROVAL : DONE;
    }

    /**
     * Fügt das Department das bei der LoginResponse dranhängt in die DepartmentListe ein
     * @param department
     */
    public void addDepartmentFromLogin(String department) {
        if(department != null) {
            Message message = new Message();
            message.setDepartment(department);
            updateDepartmentList(message);
        }
    }


    //  publish data to tableview
    public void publishTable(ObservableList<Message> messageList) {
        tableView_Table.setItems(messageList);
    }


    // opens new window when button is clicked
    public void handleMessageButton(ActionEvent event) {
        try {
          router.showModal("/MsgWindow.fxml","/theme.css",980,520,"Send a new Message");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // sets textarea to editable to edit messages
    public void handleEditButton(ActionEvent event) {
        textField_Subject.setEditable(true);
        textArea_Message.setEditable(true);
        saveButton.setDisable(false);


    }

    // shows messagecontent in textarea
    public void showMessageContent(Message message) {
        textField_Subject.setText((message==null) ? ""  : message.getSubject());
        textArea_Message.setText((message==null) ? ""  : message.getMessageText());
    }

//	private void progress_Selection(ObservableList<String> selections) {
    //	System.out.println(selections);
//	}

    // loads styles of buttons
    public void loadStyles() {
        searchButton.getStyleClass().add("search");
        msgButton.getStyleClass().add("messageButton");
        filterButton.getStyleClass().add("filter");
        removeFilterButton.getStyleClass().add("rmvFilter");
        pushButton.getStyleClass().add("pushBtn");
        addUserButton.getStyleClass().add("userBtn");
        configButton.getStyleClass().add("configBtn");

    }

    // TODO implement to get new messageObjects or messagelist from MsgWindowController




    /*
* logout - Methode schreibt dem Server eine Request indem er ein Attribut - Typ Boolean auf true setzt
* das er ausgelogt werden möchte! Bekommt er ein True zurück kann Scene gewechselt werden
* ansonsten wird eine ErroMessage ausgegeben und auf aktueller Scene geblieben!
* */
    @FXML
    protected void logout(){
        try{
            ToastView.makeMessage(router.getStage(), "Wait a moment until you are loged out!", 2500, 250, 250);
            if(loginClient.logout(true)){
                router.setSceneContent("/login.fxml","/theme.css", 500, 300);
                ToastView.makeMessage(router.getStage(), "Logout success!", 2500, 250, 250);
            }else{
                ToastView.makeErrorMessage(router.getStage(), "Login Failure! Server hasnt loged out you!", 2500, 250, 250);
            }
        }catch(WebServiceIOException e){
            ToastView.makeErrorMessage(router.getStage(), "Login Failure! ", 2500, 250, 250);
        }
        //ToastView.makeMessage(router.getStage(), "Wait a moment until you are loged out!", 2500, 250, 250);
    }
}


