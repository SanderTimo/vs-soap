package de.htwsaar.vssoap.parent;

/**
 * Created by Julian on 17.01.2017.
 * Muss übereinstimmen mit types aus der schemadefinition
 * connectionSetup.xsd
 *
 */
public enum ConnectionType {
    SERVER, CLIENT, PARENT;

    @Override
    public String toString() {
        switch(this) {
            case SERVER: return "SERVER";
            case CLIENT: return "CLIENT";
            case PARENT: return "PARENT";
            default: throw new IllegalArgumentException();
        }
    }
}
