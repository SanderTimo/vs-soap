package de.htwsaar.vssoap.parent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.ws.soap.security.wss4j2.Wss4jSecurityInterceptor;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Enthält zur Laufzeit die Verbindungsdaten unter denen
 * der Server/Client zu erreichen ist.
 *
 * server.address in den application properties setzen
 * Angabe bei gebauter Version als Parameter überschreibt application.properties
 * --server.address=
 *
 * Created by Julian on 16.03.2017.
 */
@Component
public class ConnectionConfig {

    private static Logger logger = LoggerFactory.getLogger(ConnectionConfig.class);

    @Autowired
    private Wss4jSecurityInterceptor interceptor;

    @Value("${server.port}")
    String serverPort;
    @Value("${server.address}")
    String serverAddressFromSettings;

    private String serverAddress;


    private String ownUrl;
    private String parentUrl;
    private int rang;

    private static final  String location = "/vssoap";

    public String getServerAddress(){
        if(this.serverAddress!=null && !this.serverAddress.isEmpty()){
            return this.serverAddress;
        }
        if(this.serverAddressFromSettings!=null && !this.serverAddressFromSettings.isEmpty()){
            this.serverAddress=serverAddressFromSettings;
        } else {
            try {
                //wenn weder durch application.properties noch als arg --server.address eine adresse vorliegt
                logger.warn("Achtung: Hostadresse nicht angegeben! Automatische Erkennung wählt beliebiges NetzwerkInterface.");
                logger.warn("Hostadresse setzen per run configuration: -Dserver.address=xxx.xxx.xxx.xxx");
                logger.warn("Hostadresse setzen per application.properties: server.address=xxx.xxx.xxx.xxx");
                logger.warn("");
                this.serverAddress = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e){
                logger.error("Error trying to automatically set host");
            }

        }
        logger.info("Discovered own IP-Adress: " + serverAddress);
        return this.serverAddress;
    }

    public String getUrl() {
        if(this.ownUrl==null){
            this.ownUrl = "http://" + getServerAddress() + ":"  + serverPort + location;
        }
        return this.ownUrl;
    }

    public String getParentUrl() { return this.parentUrl; }

    public void setParentUrl(String parentUrl) {
        this.parentUrl=parentUrl;
    }

    public String getLocation(){ return this.location; }

    public int getRang(){
        return this.rang;
    }
    public void setRang(int rang){
        this.rang=rang;
    }

    public void setCredentials(String username, String password) {
        interceptor.setSecurementUsername(username);
        interceptor.setSecurementPassword(password);
    }

    public Wss4jSecurityInterceptor getInterceptor() {
        return interceptor;
    }
}
