package de.htwsaar.vssoap.parent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

/**
 * @author cedosw mgoebel
 */
@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {
    @Autowired
    ConnectionConfig connection;

    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext context) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(context);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(servlet, connection.getLocation()+"/*");
    }

    @Bean
    public XsdSchema loginSchema() {
        return new SimpleXsdSchema(new ClassPathResource("login.xsd"));
    }

    @Bean(name = "login")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema loginSchema) {
        DefaultWsdl11Definition wsdl = new DefaultWsdl11Definition();
        wsdl.setPortTypeName("LoginPort");
        wsdl.setLocationUri(connection.getLocation());
        wsdl.setTargetNamespace("http://htwsaar.de/vssoap");
        wsdl.setSchema(loginSchema);
        return wsdl;
    }

    @Bean
    public XsdSchema connectionTriggerSchema() {
        return new SimpleXsdSchema(new ClassPathResource("triggerParentConnection.xsd"));
    }


    @Bean(name = "parentTrigger")
    public DefaultWsdl11Definition connectionTriggerWsdl11Definition(XsdSchema connectionTriggerSchema) {
        DefaultWsdl11Definition wsdl = new DefaultWsdl11Definition();
        wsdl.setPortTypeName("ParentTriggerPort");
        wsdl.setLocationUri(connection.getLocation());
        wsdl.setTargetNamespace("http://htwsaar.de/vssoap");
        wsdl.setSchema(connectionTriggerSchema);
        return wsdl;

    }

    @Bean
    public XsdSchema messageSchema() {
        return new SimpleXsdSchema(new ClassPathResource("messages.xsd"));
    }

    @Bean(name = "messages")
    public DefaultWsdl11Definition Message(XsdSchema messageSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("MessagePort");
        wsdl11Definition.setLocationUri(connection.getLocation());
        wsdl11Definition.setTargetNamespace("http://htwsaar.de/vssoap");
        wsdl11Definition.setSchema(messageSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema setParentSchema() {
        return new SimpleXsdSchema(new ClassPathResource("connectionSetup.xsd"));
    }

    @Bean(name = "setParentConnection")
    public DefaultWsdl11Definition setParentWsdl11Definition(XsdSchema setParentSchema) {
        DefaultWsdl11Definition wsdl = new DefaultWsdl11Definition();
        wsdl.setPortTypeName("setParentPort");
        wsdl.setLocationUri(connection.getLocation());
        wsdl.setTargetNamespace("http://htwsaar.de/vssoap");
        wsdl.setSchema(setParentSchema);
        return wsdl;

    }
}
