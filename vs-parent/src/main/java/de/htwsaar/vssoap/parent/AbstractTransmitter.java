package de.htwsaar.vssoap.parent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;

import javax.annotation.PostConstruct;

/**
 * @author cedosw
 */
public abstract class AbstractTransmitter extends WebServiceTemplate {

    @Autowired
    private ConnectionConfig connection;

    @Autowired
    private Jaxb2Marshaller marshaller;

    @PostConstruct
    public void init() {
        super.setInterceptors(new ClientInterceptor[] {connection.getInterceptor()});
        super.setMarshaller(marshaller);
        super.setUnmarshaller(marshaller);
    }

    @Override
    public String getDefaultUri() {
        return connection.getParentUrl();
    }

    public ConnectionConfig getConnection() {
        return connection;
    }

    public void setConnection(ConnectionConfig connection) {
        this.connection = connection;
    }
}
