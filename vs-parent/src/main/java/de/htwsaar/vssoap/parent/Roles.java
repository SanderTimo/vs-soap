package de.htwsaar.vssoap.parent;

/**
 * Created by Julian on 26.03.2017.
 */
public enum Roles {
    ADMIN, USER;

    @Override
    public String toString() {
        switch(this) {
            case ADMIN: return "ADMIN";
            case USER: return "USER";
            default: throw new IllegalArgumentException();
        }
    }

}
