package de.htwsaar.vssoap.parent;

/**
 * Created by Timo on 26.03.2017.
 */
public enum ActionType {
    DELETE, SAVE;

    @Override
    public String toString() {
        switch(this) {
            case DELETE: return "DELETE";
            case SAVE:   return "SAVE";
            default: throw new IllegalArgumentException();
        }
    }
}
