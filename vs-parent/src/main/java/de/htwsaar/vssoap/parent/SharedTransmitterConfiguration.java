package de.htwsaar.vssoap.parent;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.soap.security.wss4j2.Wss4jSecurityInterceptor;

@Configuration
public  class SharedTransmitterConfiguration {

    @Bean
    Wss4jSecurityInterceptor securityInterceptor() {
        Wss4jSecurityInterceptor interceptor = new Wss4jSecurityInterceptor();
        interceptor.setSecurementActions("Timestamp UsernameToken");
        interceptor.setValidateRequest(false);
        interceptor.setValidateResponse(false);
        interceptor.setSecurementMustUnderstand(false);
        return interceptor;
    }

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("de.htwsaar.vssoap.domain");
        return marshaller;
    }
}
