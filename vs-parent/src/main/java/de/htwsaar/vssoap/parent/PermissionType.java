package de.htwsaar.vssoap.parent;

/**
 * Created by Timo on 24.01.2017.
 */
public enum PermissionType {
    PRIVATE, PUBLIC;

    @Override
    public String toString() {
        switch(this) {
            case PRIVATE: return "PRIVATE";
            case PUBLIC: return "PUBLIC";
            default: throw new IllegalArgumentException();
        }
    }
}
