package de.htwsaar.vssoap.parent;

/**
 * Created by Timo on 27.12.2016.
 */

import de.htwsaar.vssoap.domain.CreateMessageRequest;
import de.htwsaar.vssoap.domain.CreateMessageResponse;
import de.htwsaar.vssoap.domain.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.ws.WebServiceException;

@Component
public class MessageClient extends AbstractTransmitter {
    private static final Logger log = LoggerFactory.getLogger(MessageClient.class);

    public CreateMessageResponse sendMessageToUri(Message message, String uri) throws WebServiceException {
        CreateMessageRequest request = new CreateMessageRequest();
        request.setMessage(message);
        try {
            logger.debug("Setze source: " + getConnection().getUrl());
            request.getMessage().setSourceUri(getConnection().getUrl());
        } catch (Exception e) {
            log.error(e.toString());
            request.getMessage().setSourceUri("");
        }
        logger.info(request);
        logger.info(uri);
        CreateMessageResponse response = (CreateMessageResponse) marshalSendAndReceive(uri, request);
        logger.info("Message:" + response.getMessage().getMessageText());
        return response;
    }
}
