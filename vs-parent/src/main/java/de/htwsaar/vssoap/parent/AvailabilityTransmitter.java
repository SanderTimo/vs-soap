package de.htwsaar.vssoap.parent;

import de.htwsaar.vssoap.domain.GetAvailabilityRequest;
import de.htwsaar.vssoap.domain.GetAvailabilityResponse;
import org.springframework.stereotype.Component;

/**
 * Created by Timo on 25.03.2017.
 */

/**
 * Diese Klasse überprüft ob man noch verbindung mit dem Server/Client  hat
 * Sie schickt eine einfache abfrage gegen den Server und schaut ob diese zurückkommt
 */
@Component
public class AvailabilityTransmitter extends AbstractTransmitter {
    public GetAvailabilityResponse sendAvailabilityRequestToUri(String uri) {
        GetAvailabilityRequest request = new GetAvailabilityRequest();
        GetAvailabilityResponse response = new GetAvailabilityResponse()  ;
        response.setAvailability(false);
        try{
            response = (GetAvailabilityResponse) marshalSendAndReceive(uri,request);
            return response;
        }
        catch (Exception wst){

        }
      return response;
    }
}
