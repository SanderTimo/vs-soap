package de.htwsaar.vssoap.parent;

/**
 * Created by Julian on 24.03.2017.
 */
public enum SoapMessageType {
    MESSAGE,
    DELETE,
    SUBSCRIPTION
}
