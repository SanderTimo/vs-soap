package de.htwsaar.vssoap.server.persistence;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Julian on 16.01.2017.
 */
@Entity
@Table(name = "clients")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public class Client extends MessageTarget {






    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName() + ":");
        sb.append("URL: " + getUrl() + ", ");
        return sb.toString();
    }

}
