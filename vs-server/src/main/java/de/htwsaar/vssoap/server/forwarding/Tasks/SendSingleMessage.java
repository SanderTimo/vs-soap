package de.htwsaar.vssoap.server.forwarding.Tasks;

import de.htwsaar.vssoap.domain.CreateMessageResponse;
import de.htwsaar.vssoap.domain.Message;
import de.htwsaar.vssoap.parent.*;
import de.htwsaar.vssoap.server.ConnectionHandling;
import de.htwsaar.vssoap.server.ServerData;
import de.htwsaar.vssoap.server.messaging.MessagingException;
import de.htwsaar.vssoap.server.persistence.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.ws.WebServiceException;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 *
 * Zum Verteilen von allgemeinen Soap Nachrichten
 *
 * Created by Julian on 19.03.2017.
 */
@Component
@Scope("prototype")
public class SendSingleMessage implements Runnable{


    protected static Logger logger = LoggerFactory.getLogger(SendSingleMessage.class);

    private ConnectionHandling connectionHandling;
    protected MessageClient transmitter;
    protected ForwardingDao forwardingDao;
    protected ServerData serverData;
    protected ClientDao clientDao;
    protected ServerDao serverDao;
    protected MessageDao messageDao;
    protected Forwarding forwarding;


    public SendSingleMessage(Forwarding forwarding){
        this.forwarding=forwarding;
    }

    /**
     * Prototype Beans können nicht geautowired werden.
     * @param messageClient
     * @param forwardingDao
     * @param connectionHandling
     * @param messageDao
     * @param serverDao
     * @param clientDao
     * @param serverData
     */
    public void hookup(MessageClient messageClient, ForwardingDao forwardingDao, ConnectionHandling connectionHandling, MessageDao messageDao, ServerDao serverDao, ClientDao clientDao, ServerData serverData){
        this.transmitter=messageClient;
        this.forwardingDao = forwardingDao;
        this.connectionHandling=connectionHandling;
        this.messageDao=messageDao;
        this.clientDao=clientDao;
        this.serverDao = serverDao;
        this.serverData=serverData;
    }


    /**
     * Abarbeitung eines Sendetasks
     */
    @Override
    public void run(){
        //Evaluate recipient and active status
        MessageTarget recipient = null;
        switch (forwarding.getConnectionType()) {
            default:
            case PARENT:
                if(!serverData.isParentActive()){
                    return;
                }
                recipient=serverData.getParent();
                break;
            case CLIENT:
                Optional<Client> oClient = clientDao.findByGuid(forwarding.getRecipientId());
                if(oClient.isPresent()){
                    if(!oClient.get().getIsActive()){
                        return;
                    }
                    recipient = oClient.get();
                }
                break;
            case SERVER:
                Optional<Server> oServerInfo = serverDao.findByGuid(forwarding.getRecipientId());
                if(oServerInfo.isPresent()){
                    if(!oServerInfo.get().getIsActive()){
                        return;
                    }
                    recipient = oServerInfo.get();
                }
                break;
        }

        //Formulate Message from Forwarding object:
        //evaluate messagecontent from DB
        //mark es 'delete' for deletions
        CreateMessageResponse response;
        try {
            switch (forwarding.getSoapMessageType()) {
                default:
                case SUBSCRIPTION:
                case MESSAGE:
                    Optional<MessagePersistence> oMessage = messageDao.findByGuid(forwarding.getSoapMessageId());
                    if(!oMessage.isPresent()){
                        clearForwarding(forwarding);
                        return;
                    }
                    MessagePersistence message=oMessage.get();
                    if(message.getSourceIp().equals(recipient.getUrl())){
                        //special case for clients: send back to original sender
                        //don't do this for servers
                        if(!forwarding.getConnectionType().equals(ConnectionType.CLIENT)) {
                            clearForwarding(forwarding);
                            return;
                        }
                    }

                    //send:
                    response = transmitter.sendMessageToUri(MessagePersistence.toSoapMessage(message), recipient.getUrl());
                    handleResponse(response);
                    clearForwarding(forwarding);
                    break;
                case DELETE:
                    Message emptymessage = new Message();
                    emptymessage.setGuid(forwarding.getSoapMessageId());
                    emptymessage.setActionType("DELETE");
                    emptymessage.setMessageText("");  //damit nicht null drinsteht
                    logger.debug("Delete " + forwarding.getSoapMessageId() + " an " + recipient.getUrl());

                    //send:
                    response = transmitter.sendMessageToUri(emptymessage, recipient.getUrl());
                    handleResponse(response);
                    clearForwarding(forwarding);
                    break;
            }

        } catch (MessagingException | WebServiceException messagingError) {
            //Message could not be delivered: mark target as inactive to stop further messages
            if(recipient!=null)
                logger.warn("Kann Message Empfänger +" + recipient.getUrl() + " nicht erreichen. " + messagingError.getLocalizedMessage() );
            if(forwarding!=null && forwarding.getConnectionType()!=null)
                connectionHandling.markInactive(recipient.getId(), forwarding.getConnectionType());
        }
    }

    /**
     * Prüfen ob in der Rückgabe ein Fehler gesetzt wurde
     * @param message
     * @throws MessagingException
     */
    public void handleResponse(CreateMessageResponse message) throws MessagingException {
        CreateMessageResponse response = message;
         if(ActionType.DELETE.toString().equals(response.getMessage().getActionType())){
            //gilt hier als erfolg: messagecontent wurde entfernt
            return;
        }
        // Prevents Nullpointer, if messageText is null
        String messageText = response.getMessage().getMessageText();
        if(messageText != null && messageText.contains("Beim Senden der Message ist ein Fehler aufgetreten.")){
            throw new MessagingException("Fehler beim Senden der Nachricht: "+ response.getMessage().getMessageText());
        }
    }

    @Transactional
    private void clearForwarding(Forwarding forwarding){
        try{
            Forwarding forwarding1 = forwardingDao.findOne(forwarding.getId());
            if(forwarding1!=null)
                forwardingDao.delete(forwarding1);
        } catch (IllegalArgumentException e){
            //might already be deleted
            //no error
        }
    }

}
