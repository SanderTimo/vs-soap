package de.htwsaar.vssoap.server.persistence;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author cedosw mgoebel
 */

@Entity
@Table(name = "users")
public class CustomUser extends AbstractEntity {
    @NotEmpty
    @Column(unique = true)
    private String username;

    @NotEmpty
    private String password;

    @NotEmpty
    private String role;

    //private BCryptPasswordEncoder encoder;

    public CustomUser() {

    }

    public CustomUser(String username, String password, String role) {
        //encoder = new BCryptPasswordEncoder(10);
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        //String hashedPW = encoder.encode(password);
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
