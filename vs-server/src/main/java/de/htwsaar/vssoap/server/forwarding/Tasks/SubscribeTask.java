package de.htwsaar.vssoap.server.forwarding.Tasks;

import de.htwsaar.vssoap.parent.ConnectionType;
import de.htwsaar.vssoap.parent.MessageClient;
import de.htwsaar.vssoap.parent.PermissionType;
import de.htwsaar.vssoap.parent.SoapMessageType;
import de.htwsaar.vssoap.server.ConnectionHandling;
import de.htwsaar.vssoap.server.ServerData;
import de.htwsaar.vssoap.server.persistence.MessageTarget;
import de.htwsaar.vssoap.server.forwarding.sender.SendWorkqueue;
import de.htwsaar.vssoap.server.persistence.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Wird ein subscribetask angelegt werden von diesem Server alle vorhanden
 * Messages an den recipient geschickt
 * Created by Julian on 16.01.2017.
 */
@Component
@Scope("prototype")
public class SubscribeTask implements Runnable {


    protected static Logger logger = LoggerFactory.getLogger(SubscribeTask.class);

    protected MessageTarget recipient;

    protected ConnectionType connectionType;

    @Autowired
    protected SendWorkqueue workqueue;

    @Autowired
    protected MessageDao messageDao;

    @Autowired
    protected MessageClient transmitter;

    @Autowired
    protected SendTaskFactory factory;

    @Autowired
    protected ServerData serverData;

    @Autowired
    MessageClient messageClient;

    @Autowired
    ConnectionHandling connectionHandling;

    @Autowired
    ForwardingDao forwardingDao;

    public void setRecipient(MessageTarget recipient) {
        this.recipient = recipient;
    }

    public void setConnectionType(ConnectionType type) {
        this.connectionType = type;
    }

    /**
     * Erstellt eine Liste aller für den Verbindungstyp vorgesehenen Messages
     * Server: alle public
     * Client: alle private & public
     * Parent: rang kleiner eigener rang, die nicht vom parent hier abgelegt wurden
     *
     * @param connectionType
     * @return
     */
    public boolean subscribe(ConnectionType connectionType) {
        if (recipient == null) {
            logger.error("ForwardingFehler: Kein Ziel für Message gesetzt");
            return false;
        }

        logger.info("Subscription received form: " + recipient.getUrl());
        Iterable<MessagePersistence> messages = messageDao.findAll();;
        Optional<List<MessagePersistence>> oMessages;
        switch(connectionType){
            default:
            case SERVER:
                oMessages = messageDao.findByType(PermissionType.PUBLIC.toString());
                if(oMessages.isPresent()) {
                    messages=oMessages.get();
                }
                logger.debug("Messgecount (public)" + oMessages.get().size());
                break;
            case CLIENT:
                logger.debug("Messagecount:" + messageDao.count());
                break;
            case PARENT:
                oMessages = messageDao.findAllByRangLessThanAndSourceIpNot(serverData.getRang(), serverData.getParentUrl());
                if(oMessages.isPresent()) {
                    messages=oMessages.get();
                }
                logger.debug("Messgecount (zu Parent)" + oMessages.get().size());
                break;
        }


        //remove already staged forwards, replace with forward all
        forwardingDao.deleteByRecipientId(recipient.getGUID());

        for (MessagePersistence message : messages) {
            Forwarding forwarding = new Forwarding();
            forwarding.setSoapMessageType(SoapMessageType.SUBSCRIPTION);
            forwarding.setConnectionType(connectionType);
            forwarding.setSoapMessageId(message.getGUID());
            forwarding.setRecipientId(recipient.getGUID());
            forwardingDao.save(forwarding);

        }

        return true;

    }

    public void run() {
        subscribe(connectionType);
    }

}
