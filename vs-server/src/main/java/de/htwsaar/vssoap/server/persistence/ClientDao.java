package de.htwsaar.vssoap.server.persistence;


import org.springframework.data.repository.CrudRepository;

import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * Created by Julian on 15.01.2017.
 */
@Transactional
public interface ClientDao extends CrudRepository<Client, Integer> {

    Optional<Client> findByUrl(String Url);
    Optional<Client> findByGuid(String id);
    Optional<List<Client>> findByIsActive(Boolean isActive);


}