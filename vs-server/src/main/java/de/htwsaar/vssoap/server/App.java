package de.htwsaar.vssoap.server;

import de.htwsaar.vssoap.domain.ParentConnectionResponse;
import de.htwsaar.vssoap.parent.AbstractJavaFxApp;
import de.htwsaar.vssoap.parent.ConnectionType;
import de.htwsaar.vssoap.server.forwarding.Scheduling.ScheduledSender;
import de.htwsaar.vssoap.server.messaging.MessagingException;
import de.htwsaar.vssoap.server.messaging.ParentConnectionTransmitter;
import de.htwsaar.vssoap.server.persistence.ClientDao;
import de.htwsaar.vssoap.server.persistence.ServerDao;
import javafx.application.Preloader;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Lazy;
import org.springframework.ws.WebServiceException;



/**
 * Die Main Klasse der Applikation. Erbt von der abstrakten JavaFx-Oberklasse um Spring-Support
 * auch in JavaFX-Klassen zu gewährleisten.
 *
 * @author cedosw
 */
@Lazy
@SpringBootApplication
@ComponentScan({"de.htwsaar.vssoap.parent","de.htwsaar.vssoap.server" })
public class App extends AbstractJavaFxApp {

    private static Logger logger = LoggerFactory.getLogger(App.class);


    @Value("${ui.server.title}")
    private String windowTitle;

    @Autowired
    ServerData connection;

    @Autowired
    ClientDao clientDao;

    @Autowired
    ServerDao serverDao;

    @Autowired
    ServerData serverData;

    @Autowired
    ParentConnectionTransmitter transmitter;

    @Autowired
    ScheduledSender sender;


    /**
     * Startet die JavaFX Stage.
     *
     * @param stage Die MainStage der Anwendung.
     * @throws Exception Exceptions während dem Starten der JavaFX Stage.
     */
    @Override
    public void start(Stage stage) throws Exception {



        notifyPreloader(new Preloader.StateChangeNotification(Preloader.StateChangeNotification.Type.BEFORE_START));
        logger.info("Server App has been started on:" + connection.getUrl());

        serverData.readConfigDb();
        logger.debug("Server App has Parent: " + connection.getParentUrl());
        logger.debug(serverData.ServerStateInfo());

        try{
            if((connection.getParentUrl()!=null) && !(connection.getParentUrl().isEmpty())){
                //Bei Serverneustart nochmal am Parent melden
                ParentConnectionResponse response = transmitter.sendConnectionRequest(connection.getParentUrl());
            }
        } catch (MessagingException | WebServiceException wse){
            logger.error("Verbindung zum Parent bei Serverstart fehlgeschlagen.");
            serverData.setParentActive(false);
        }


    }





    /**
     * Die Main Methode des Programms.
     *
     * @param args Kommandozeilenparameter.
     */
    public static void main(String args[]) {
        launchApp(App.class, args);
    }

}