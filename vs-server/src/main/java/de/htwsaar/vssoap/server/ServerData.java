package de.htwsaar.vssoap.server;

import de.htwsaar.vssoap.parent.ConnectionConfig;
import de.htwsaar.vssoap.server.persistence.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Optional;

/**
 *
 * Enthält die Config/Connectiondaten zur Laufzeit
 * Synchroniziert Daten mit der ConnectionConfig aus dem VS-Parent Projekt
 *
 *
 * Created by Julian on 16.01.2017.
 */
@Component
public class ServerData {

    private static Logger logger = LoggerFactory.getLogger(ServerData.class);

    @PostConstruct
    public void setSecretToken() {
        connection.setCredentials("secret_server", "secret777");
    }

    @Autowired
    private ServerConfigDao serverConfigDao;

    @Autowired
    private ConnectionConfig connection;

    private MessageTarget parentMessageTarget;

    @Autowired
    private ClientDao clientDao;

    @Autowired
    private ServerDao serverDao;

    private boolean parentActive = true;


    public void init(){
        //load existing config from db
    }

    public Boolean isParentActive(){
        return parentActive;
    }

    public void setParentActive(boolean isactive){
        ServerConfig config = readConfigDb();
        config.setIsActive(isactive);
        this.parentActive=isactive;
        serverConfigDao.save(config);

    }

    public ServerConfig readConfigDb(){
        Optional<ServerConfig> serverConfig = serverConfigDao.findTopByOrderById();
        if(serverConfig.isPresent()){
                connection.setRang(serverConfig.get().getRang());
                serverConfig.get().setParentActive(true);
                serverConfig.get().setUrl(connection.getUrl());
                connection.setParentUrl(serverConfig.get().getParentUrl());
                serverConfigDao.save(serverConfig.get());
            return serverConfig.get();
        } else {
            logger.info("Creating new Config entry: ");
            ServerConfig si = new ServerConfig();
            si.setUrl(connection.getUrl());
            si.setGUID((new GUID()).generateGUID());
            si.setParentUrl(connection.getParentUrl());
            si.setRang(0);
            serverConfigDao.save(si);
            return si;
        }
    }

    public String getUrl() {
        return connection.getUrl();
    }

    /**
     * Erlaubt den Zugriff auf den Parent als MessageTarget
     * Dadurch kann der Parent genau wie die Clients/Server
     * als Ziel gesetzt werden
     *
     * @return
     */
    public MessageTarget getParent(){
        if(this.parentMessageTarget==null || this.parentMessageTarget.getUrl() !=this.getParentUrl()) {
            this.parentMessageTarget = new MessageTarget(connection.getParentUrl(), serverConfigDao.findTopByOrderById().get().getGUID()) {
            };
        }
        return this.parentMessageTarget;
    }

    public String getParentUrl() {
        return connection.getParentUrl();
    }

    public void setParentUrl(String parentUrl) {
        connection.setParentUrl(parentUrl);
        ServerConfig config = readConfigDb();
        config.setParentUrl(parentUrl);
        serverConfigDao.save(config);
    }

    public int getRang(){
        return readConfigDb().getRang();
    }

    public void setRang(int rang){
        connection.setRang(rang);
        ServerConfig config = readConfigDb();
        config.setRang(rang);
        serverConfigDao.save(config);
    }

    public String getDepartment(){
        return readConfigDb().getDepartment();
    }


    public String ServerStateInfo(){
        StringBuilder sb = new StringBuilder();
        sb.append("\nClients in der DB:\n");
        clientDao.findAll().forEach((client) -> sb.append(client.toString() + "\n"));
        sb.append("Kind Server in der DB:\n");
        serverDao.findAll().forEach((child) -> sb.append(child.toString() + "\n"));
        ServerConfig serverConfig = readConfigDb();
        sb.append(serverConfig.toString());
        return sb.toString();
    }



}
