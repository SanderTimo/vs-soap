package de.htwsaar.vssoap.server.messaging;


import de.htwsaar.vssoap.domain.GetLoginRequest;
import de.htwsaar.vssoap.domain.GetLoginResponse;
import de.htwsaar.vssoap.domain.GetLogoutRequest;
import de.htwsaar.vssoap.domain.GetLogoutResponse;
import de.htwsaar.vssoap.parent.ConnectionType;
import de.htwsaar.vssoap.server.ConnectionHandling;
import de.htwsaar.vssoap.server.ServerData;
import de.htwsaar.vssoap.server.persistence.CustomUser;
import de.htwsaar.vssoap.server.persistence.CustomUserDao;
import de.htwsaar.vssoap.server.persistence.ServerConfigDao;
import org.hibernate.validator.internal.util.logging.Log_$logger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.transport.context.TransportContext;
import org.springframework.ws.transport.context.TransportContextHolder;
import org.springframework.ws.transport.http.HttpServletConnection;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * @author cedosw mgoebel
 */
@Endpoint
public class LoginEndpoint {

    private static final String namespace = "http://htwsaar.de/vssoap/domain";
    private static Logger logger = LoggerFactory.getLogger(LoginEndpoint.class);

    @Autowired
    private CustomUserDao customUserDao;
    @Autowired
    ServerData serverData;

    @Autowired
    ConnectionHandling connectionHandling;

    @PayloadRoot(namespace = namespace, localPart = "getLoginRequest")
    @ResponsePayload
    public GetLoginResponse login(@RequestPayload GetLoginRequest loginRequest) {
        GetLoginResponse resp = new GetLoginResponse();
        Optional<CustomUser> user = customUserDao.findByUsername(loginRequest.getUsername());
        /*
         * Überprüft ob es in der Datenbank den Username gibt.
         */
        if(user.isPresent()) {
            /*
             * Überprüft ob das Passwort in der Datenbank gleich dem aus dem Request ist.
             */
            if(user.get().getPassword().equals(loginRequest.getPassword())){
                resp.setRole(user.get().getRole());
                resp.setDepartment(serverData.getDepartment());
                return resp;
            }else{
                resp.setRole("Password ist falsch!");
                return resp;
            }
        }
        resp.setRole("Username gibt es nicht!");
        return resp;
    }

    @PayloadRoot(namespace = namespace, localPart = "getLogoutRequest")
    @ResponsePayload
    public GetLogoutResponse login(@RequestPayload GetLogoutRequest logoutRequest) {

        logger.info("Ausgloggt: " + logoutRequest.getUri());
        connectionHandling.delete(logoutRequest.getUri(), ConnectionType.CLIENT);

        GetLogoutResponse resp = new GetLogoutResponse();

        resp.setLogout(true);
        return resp;
    }
}
