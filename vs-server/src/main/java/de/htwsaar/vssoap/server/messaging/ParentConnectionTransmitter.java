package de.htwsaar.vssoap.server.messaging;

import de.htwsaar.vssoap.domain.ParentConnectionRequest;
import de.htwsaar.vssoap.domain.ParentConnectionResponse;
import de.htwsaar.vssoap.parent.AbstractTransmitter;
import de.htwsaar.vssoap.parent.ConnectionType;
import de.htwsaar.vssoap.server.ConnectionHandling;
import de.htwsaar.vssoap.server.ServerData;
import de.htwsaar.vssoap.server.persistence.Server;
import de.htwsaar.vssoap.server.persistence.ServerDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.WebServiceException;

/**
 *
 * Transmitter Klasse zum Absenden von "ParentConnection" Anfragen.
 * Mit einem ParentConnectionRequest meldet sich ein Server am Vater an,
 * falls ParentConnectionResponse erhalten wird.
 *
 * @author JulianW
 */
@Component
public class ParentConnectionTransmitter extends AbstractTransmitter {

    @Autowired
    ConnectionHandling connectionHandling;

    @Autowired
    ServerData serverData;

    @Autowired
    ServerDao serverDao;

    private static final Logger logger = LoggerFactory.getLogger(ParentConnectionTransmitter.class);

    public ParentConnectionResponse sendConnectionRequest(String parentUrl) throws MessagingException{

        ParentConnectionRequest request = new ParentConnectionRequest();
        request.setType(ConnectionType.SERVER.toString());
        request.setChildUrl(getConnection().getUrl());
        request.setParentUrl(parentUrl);
        ParentConnectionResponse response;

        if(parentUrl==null){
            throw new MessagingException("Invalid Parent: null");
        }

        if(getConnection().getUrl().equals(parentUrl)){
           throw new MessagingException("Invalid Parent: self");
        }
        Iterable<Server> iChildren = serverDao.findAll();

        for (Server child : iChildren) {
            if(parentUrl.equals(child.getUrl())){
                throw new MessagingException("Invalid Parent: child server");
            }
        }
        //messages an alten parent stoppen.

        connectionHandling.delete(getConnection().getParentUrl(), ConnectionType.PARENT);
        try {

            serverData.setParentUrl(parentUrl);
            response = sendRequest(request);
            serverData.setRang(response.getRang().intValue());
            logger.info(response.toString());
        }catch(WebServiceException e){
            connectionHandling.markInactive(0,ConnectionType.PARENT);
            //forward error to have it handled by the sender
            throw new MessagingException(e);
        }
        return response;
    }

    private ParentConnectionResponse sendRequest(ParentConnectionRequest request) {
        return (ParentConnectionResponse) marshalSendAndReceive(request.getParentUrl(), request);
    }
}
