package de.htwsaar.vssoap.server.persistence;

import de.htwsaar.vssoap.server.GUID;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;

/**
 * @author JulianW
 */

@Entity
@Table(name = "servers")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public class Server extends MessageTarget{

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName() + ":");
        sb.append("URL: " + getUrl() + ", ");
       return sb.toString();
    }

}
