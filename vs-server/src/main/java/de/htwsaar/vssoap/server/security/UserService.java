package de.htwsaar.vssoap.server.security;

import de.htwsaar.vssoap.server.persistence.CustomUser;
import de.htwsaar.vssoap.server.persistence.CustomUserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * @author cedosw (22.03.2017)
 */
@Component
public class UserService implements UserDetailsService {

    private static Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private CustomUserDao customUserDao;

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<CustomUser> user = customUserDao.findByUsername(username);
        CustomUser customUser;
        if(user.isPresent()) {
            logger.info("Found user: " + username);
            customUser = user.get();
        } else if (username.equals("secret_server")) {
             customUser = new CustomUser("secret_server", "secret777", "server");
        } else {
            logger.info(username + " not found.");
            return null;
        }
        return new User(customUser.getUsername(), customUser.getPassword(), getAuthorities(customUser));
    }

    private Set<GrantedAuthority> getAuthorities(CustomUser customUser){
        Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
        GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(customUser.getRole());
        authorities.add(grantedAuthority);
        logger.info("User has Role " + customUser.getRole());
        return authorities;
    }

}
