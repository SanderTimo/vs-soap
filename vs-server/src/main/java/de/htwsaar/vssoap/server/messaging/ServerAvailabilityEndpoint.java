package de.htwsaar.vssoap.server.messaging;

import de.htwsaar.vssoap.domain.GetAvailabilityRequest;
import de.htwsaar.vssoap.domain.GetAvailabilityResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 * Created by Timo on 25.03.2017.
 */
@Endpoint
public class ServerAvailabilityEndpoint {

    private static final String NAMESPACE_URI = "http://htwsaar.de/vssoap/domain";


    private static Logger logger = LoggerFactory.getLogger(MessageEndpoint.class);

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAvailabilityRequest")
    @ResponsePayload
    public GetAvailabilityResponse getMessage(@RequestPayload GetAvailabilityRequest request) {

        GetAvailabilityResponse response = new GetAvailabilityResponse();
        response.setAvailability(true);

        return response;


    }


}
