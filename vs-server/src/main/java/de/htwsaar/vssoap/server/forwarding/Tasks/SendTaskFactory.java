package de.htwsaar.vssoap.server.forwarding.Tasks;

import de.htwsaar.vssoap.parent.ConnectionType;
import de.htwsaar.vssoap.parent.SoapMessageType;
import de.htwsaar.vssoap.server.ServerData;
import de.htwsaar.vssoap.server.persistence.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.Optional;

/**
 * Weiterleitung von Messages an Kind/Parent/Server
 * Zum Anlegen von MessageTasks die asynchron abgearbeitet werden
 *
 *
 * Created by Julian on 15.01.2017.
 */
@Component
public class SendTaskFactory {
    private static Logger logger = LoggerFactory.getLogger(SendTaskFactory.class);

    @Autowired
    ServerDao serverDao;

    @Autowired
    ServerData serverData;

    @Autowired
    ForwardingDao forwardingDao;

    @Autowired
    ClientDao clientDao;


    public void createMessageParentTask(MessagePersistence messagePersistence){
        logger.debug("Richte weiterleitung zum Parent ein für Message" + messagePersistence.getId());
        if(serverData.getParentUrl()==null){
            //Konfigurationsfehler, kein Softwarefehler
            logger.warn("Achtung Message mit kleinerem Rang kann nicht weitergeleitet werden, weil kein Parent eingetragen ist.");
            return;
        }
        Forwarding forwarding = new Forwarding();
        forwarding.setRecipientId(serverData.readConfigDb().getGUID());
        forwarding.setSoapMessageId(messagePersistence.getGUID());
        forwarding.setSoapMessageType(SoapMessageType.MESSAGE);
        forwarding.setConnectionType(ConnectionType.PARENT);
        forwardingDao.save(forwarding);

    }

    public void createMessageChildrenTask(MessagePersistence messagePersistence){
        logger.debug("Richte Weiterleitung zu Children ein für Message" + messagePersistence.getId());
        Iterable<Server> children = serverDao.findAll();
        forward(children.iterator(), messagePersistence, ConnectionType.SERVER);
    }

    public void createMessageClientsTask(MessagePersistence messagePersistence){
        logger.debug("Richte Weiterleitung zu Clients ein für Message" + messagePersistence.getId());
        Iterable<Client> clients = clientDao.findAll();
        forward(clients.iterator(), messagePersistence, ConnectionType.CLIENT);
    }

    /**
     * Erstellt ein Forwarding für eine Delete anweisung für eine Message
     * @param messagePersistence
     */
    public void createDelteMessageTask(MessagePersistence messagePersistence){
        forwardingDao.deleteBySoapMessageId(messagePersistence.getGUID());
        Iterable<Client> clients = clientDao.findAll();
        forwardDelete(clients.iterator(),messagePersistence,ConnectionType.CLIENT);
        Iterable<Server> serverInfos = serverDao.findAll();
        forwardDelete(serverInfos.iterator(),messagePersistence,ConnectionType.SERVER);
        if(serverData.getRang()==0){
            return;
        }

        Forwarding forwarding = new Forwarding();
        forwarding.setRecipientId(serverData.readConfigDb().getGUID());
        forwarding.setSoapMessageId(messagePersistence.getGUID());
        forwarding.setConnectionType(ConnectionType.PARENT);
        forwarding.setSoapMessageType(SoapMessageType.DELETE);
        Optional<Forwarding> oForwarding = forwardingDao.findOneByRecipientIdAndSoapMessageId(forwarding.getRecipientId(),forwarding.getSoapMessageId());
        if(!oForwarding.isPresent())
            forwardingDao.save(forwarding);
    }


    /**
     * Erstellt ein Forwarding für ein upsert einer Message
     * @param iterator
     * @param messagePersistence
     * @param connectionType
     */
    private void forward(Iterator<? extends MessageTarget> iterator, MessagePersistence messagePersistence, ConnectionType connectionType){
        iterator.forEachRemaining((t)->{
            //skip received_from
            if(connectionType.equals(ConnectionType.PARENT) || connectionType.equals(ConnectionType.SERVER)){
                if(t.getUrl().equals(messagePersistence.getSourceIp())){
                    return;
                }
                if(t.getUrl().equals(serverData.getUrl())){
                    // schleife auf sich selbst eingerichtet. lieber nicht senden
                    return;
                }
            }
            Forwarding forwarding = new Forwarding();
            forwarding.setRecipientId(t.getGUID());
            forwarding.setSoapMessageId(messagePersistence.getGUID());
            forwarding.setConnectionType(connectionType);
            forwarding.setSoapMessageType(SoapMessageType.MESSAGE);
            Optional<Forwarding> oForwarding = forwardingDao.findOneByRecipientIdAndSoapMessageId(forwarding.getRecipientId(),forwarding.getSoapMessageId());
            if(!oForwarding.isPresent())
                forwardingDao.save(forwarding);
            //else: forwarding bereits eingerichtet
        });
    }

    /**
     * Stellt ein Forwarding ein das Löschen einer Nachricht
     * @param iterator
     * @param messagePersistence
     * @param connectionType
     */
    private void forwardDelete(Iterator<? extends MessageTarget> iterator, MessagePersistence messagePersistence, ConnectionType connectionType){
        iterator.forEachRemaining((t)->{
            //skip received_from for deletes
            if( connectionType.equals(ConnectionType.SERVER)){
                if(t.getUrl().equals(serverData.getUrl())){
                    return;
                }
            }
            if(t.getUrl().equals(serverData.getUrl())){
                // schleife auf sich selbst eingerichtet. lieber nicht senden
                return;
            }
            Forwarding forwarding = new Forwarding();
            forwarding.setRecipientId(t.getGUID());
            forwarding.setSoapMessageId(messagePersistence.getGUID());
            forwarding.setConnectionType(connectionType);
            forwarding.setSoapMessageType(SoapMessageType.DELETE);
            Optional<Forwarding> oForwarding = forwardingDao.findOneByRecipientIdAndSoapMessageId(forwarding.getRecipientId(),forwarding.getSoapMessageId());
            if(!oForwarding.isPresent())
                forwardingDao.save(forwarding);
        });
    }


}
