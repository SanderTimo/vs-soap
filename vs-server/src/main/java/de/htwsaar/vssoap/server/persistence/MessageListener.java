package de.htwsaar.vssoap.server.persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

/**
 * Created by Julian on 29.03.2017.
 */

public class MessageListener {

    private static Logger logger = LoggerFactory.getLogger(MessageListener.class);

    @PrePersist
    @PreUpdate
    public void onUpdate(MessagePersistence message){
        logger.debug("Create/Upate: " +message.toString());
    }

    @PreRemove
    public void onDelete(MessagePersistence messagePersistence){
        logger.debug("Delted: " + messagePersistence.toString());
    }
}
