package de.htwsaar.vssoap.server.messaging;


import de.htwsaar.vssoap.domain.*;
import de.htwsaar.vssoap.parent.ConnectionType;
import de.htwsaar.vssoap.server.ServerData;
import de.htwsaar.vssoap.server.forwarding.sender.SendWorkqueue;
import de.htwsaar.vssoap.server.persistence.CustomUserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.math.BigInteger;

/**
 * Ausgelöst vom Superuser in der Oberfläche wird die Verbindung zu
 * einem Parentserver eingeleitet.
 * Es wird eine Connectionanfrage an den Parentserver angestoßen
 *
 * @author Jweiland
 */
@Endpoint
public class ParentSetupTriggerEndpoint {
    private static Logger logger = LoggerFactory.getLogger(ParentSetupTriggerEndpoint.class);

    @Autowired
    private  ServerData connection;

    @Autowired
    private ParentConnectionTransmitter transmitter;

    @Autowired
    private SendWorkqueue workqueue;

    private static final String namespace = "http://htwsaar.de/vssoap/domain";

    /**
     * Stößt Registrierungsanfrage an einen Parentserver an.
     * Response enthält BAD_REQUEST wenn der Request ungültig ist.
     * Status OK garantiert nicht, dass der nachfolgende ConnectionRequest zum
     * Parent ebenfalls erfolgreich war.
     *
     * @param request
     * @return
     */
    @PayloadRoot(namespace = namespace, localPart = "parentSetupTriggerRequest")
    @ResponsePayload
    public ParentSetupTriggerResponse setParent(@RequestPayload ParentSetupTriggerRequest request) {
        ParentSetupTriggerResponse resp = new ParentSetupTriggerResponse();

        if(request.getParentUrl().isEmpty()){
            //invalid parent: no address given
            logger.error("Invalid Parent Trigger received: missing parent URL");
            resp.setStatus(HttpStatus.BAD_REQUEST.toString());
            return resp;
        }
        if(request.getParentUrl().equals(connection.getUrl())){
            //invalid parent: eigene adresse!
            logger.error("Invalid Parent Trigger received: cannot set own address as parent");
            resp.setStatus(HttpStatus.BAD_REQUEST.toString());
            return resp;
        }
        addConnectionRequestToWorkqueue(request);
        resp.setStatus(HttpStatus.OK.toString());
        return resp;
    }

    /**
     * Auftrag ConnectionRequest durchzuführen wird in der Workqueue
     * abgelegt und asynchron abgearbeitet
     *
     *
     * @param request
     */
    private void addConnectionRequestToWorkqueue(ParentSetupTriggerRequest request){
        Runnable t1 = () -> {
            logger.info("Sende Connection Anfrage von: " + connection.getUrl() + " an: " +request.getParentUrl());
            transmitter.setDefaultUri(request.getParentUrl());
            try {
                ParentConnectionResponse response = transmitter.sendConnectionRequest(request.getParentUrl());
                logger.info("Connection hergestellt. Rang erhalten: " + response.getRang());
            } catch (Exception e){
                logger.error("Fehler beim Verbindungsaufbau zum Parent Server",e);

            }
        };
        workqueue.addTask(t1);
    }

}
