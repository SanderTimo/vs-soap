package de.htwsaar.vssoap.server.forwarding.Scheduling;

import de.htwsaar.vssoap.domain.GetAvailabilityResponse;
import de.htwsaar.vssoap.parent.AvailabilityTransmitter;
import de.htwsaar.vssoap.parent.ConnectionType;
import de.htwsaar.vssoap.server.ServerData;
import de.htwsaar.vssoap.server.persistence.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;
import java.util.Optional;

/**
 * In regelmäßigem Abstand werden inaktive MessageZiele kontaktiert
 * Antwortet das Ziel wieder wird es als aktiv markiert.
 * An aktive Ziele sender der ScheduledSender regelmäßig
 * Created by Julian on 21.03.2017.
 */
@Configuration
@EnableAsync
@EnableScheduling
public class ConnectionRetry {


    private static Logger logger = LoggerFactory.getLogger(ConnectionRetry.class);
    @Autowired
    ClientDao clientDao;

    @Autowired
    AvailabilityTransmitter availabilityTransmitter;

    @Autowired
    ServerDao serverDao;

    @Autowired
    ServerData serverData;

    @Scheduled(initialDelay = 2000, fixedDelay = 45000)
    public void retryAllConnections(){
        Optional<List<Client>> inactiveClientsOpt = clientDao.findByIsActive(false);
        if(inactiveClientsOpt.isPresent()) {
            inactiveClientsOpt.get().forEach((client) -> {
                retryConnection(client.getUrl(), ConnectionType.CLIENT);
            });
        }
        Optional<List<Server>> inactiveServersOpt = serverDao.findByIsActive(false);
        if(inactiveServersOpt.isPresent()) {
            inactiveServersOpt.get().forEach((server) -> {
                retryConnection(server.getUrl(),ConnectionType.SERVER);
            });
        }
        if(!serverData.isParentActive()){
            retryConnection(serverData.getParentUrl(),ConnectionType.PARENT);
        }
    }

    @Async
    public void retryConnection(String url, ConnectionType type){

        logger.debug("Sende eine Verbindungsanfrage an inaktives Messaging Ziel:" + url);
        GetAvailabilityResponse response = availabilityTransmitter.sendAvailabilityRequestToUri(url);
        if(response.isAvailability()) {
            switch (type) {
                case CLIENT:
                Optional<Client> activeClient = clientDao.findByUrl(url);
                if (activeClient.isPresent()) {
                    activeClient.get().setIsActive(true);
                    clientDao.save(activeClient.get());
                    logger.info("Client wieder erreichbar: " + activeClient.get().getUrl());
                }
                break;
                case SERVER:
                    Optional<Server> activeServerInfo = serverDao.findByUrl(url);
                    if (activeServerInfo.isPresent()) {
                        activeServerInfo.get().setIsActive(true);
                        serverDao.save(activeServerInfo.get());
                        logger.info("Server wieder erreichbar " + activeServerInfo.get().getUrl());
                    }
                break;
                case PARENT:
                    serverData.setParentActive(true);
                    logger.info("Parent wieder erreichbar " + serverData.getParentUrl());
            }
        }
    }

}
