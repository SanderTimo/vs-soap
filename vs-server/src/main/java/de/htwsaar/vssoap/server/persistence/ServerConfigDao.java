package de.htwsaar.vssoap.server.persistence;


import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * Created by Julian on 16.03.2017.
 */
@Transactional
public interface ServerConfigDao extends CrudRepository<ServerConfig, Integer> {
    Optional<ServerConfig> findTopByOrderById();

}
