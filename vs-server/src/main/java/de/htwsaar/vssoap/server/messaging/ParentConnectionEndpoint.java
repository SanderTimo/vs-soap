package de.htwsaar.vssoap.server.messaging;

import de.htwsaar.vssoap.domain.ParentConnectionRequest;
import de.htwsaar.vssoap.domain.ParentConnectionResponse;
import de.htwsaar.vssoap.parent.ConnectionType;
import de.htwsaar.vssoap.server.GUID;
import de.htwsaar.vssoap.server.ServerData;
import de.htwsaar.vssoap.server.forwarding.sender.SendWorkqueue;
import de.htwsaar.vssoap.server.persistence.MessageTarget;
import de.htwsaar.vssoap.server.forwarding.Tasks.SubscribeTask;
import de.htwsaar.vssoap.server.persistence.Client;
import de.htwsaar.vssoap.server.persistence.ClientDao;
import de.htwsaar.vssoap.server.persistence.Server;
import de.htwsaar.vssoap.server.persistence.ServerDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.math.BigInteger;
import java.util.Optional;

/**
 * Nimmt Registrierungsanfragen entgegen
 * Registriert Server / Clients für Verteilung zukünftiger Messages
 * Erstellt Auftrag bestehende Messages an das neue Kind zu leiten
 *
 * @author JulianW
 */
@Endpoint
public class ParentConnectionEndpoint {

    private static final String namespace = "http://htwsaar.de/vssoap/domain";
    private static final Logger logger = LoggerFactory.getLogger(ParentConnectionEndpoint.class.getName());

    @Autowired
    private ServerDao serverDao;
    @Autowired
    private ServerData serverData;
    @Autowired
    private ClientDao clientDao;
    @Autowired
    private ServerData connection;
    @Autowired
    private SubscribeTask subscribeTask;
    @Autowired
    private SendWorkqueue workqueue;

    private GUID guid = new GUID();


    //Endpoints werden von IntelliJ fälschlicherweise als nicht verwendet markiert. nicht beachten

    /**
     * Gegen diesen Endpunk melden sich Clients und Server an
     *
     * @param request
     * @return Antwort der Anfrage: Uri des Vaters
     * @throws MessagingException
     */
    @PayloadRoot(namespace = namespace, localPart = "ParentConnectionRequest")
    @ResponsePayload
    public ParentConnectionResponse receiveRequest(@RequestPayload ParentConnectionRequest request) throws MessagingException{
        logger.debug("Connection requested from" + request.getChildUrl());
        ParentConnectionResponse resp = new ParentConnectionResponse();
        resp.setParentUrl(connection.getUrl());

        MessageTarget child;

        //Registrieren in DB+ Antwort Rang setzen
        if (ConnectionType.SERVER.toString().equals(request.getType())) {
            child = registerChild(request);
            //server haben rang+1
            resp.setRang(BigInteger.valueOf(serverData.getRang() + 1));
            subscribeTask.setConnectionType(ConnectionType.SERVER);
        } else if (ConnectionType.CLIENT.toString().equals(request.getType())) {
            child = registerClient(request);
            //clients haben selben rang
            resp.setRang(BigInteger.valueOf(serverData.getRang()));
            subscribeTask.setConnectionType(ConnectionType.CLIENT);
        } else {
            throw new MessagingException("Missing ConnectionType for ConnectionRequest");
        }


        subscribeTask.setRecipient(child);
        workqueue.addTask(subscribeTask);

        return resp;
    }

    /**
     * Registrierung eines Kindservers an diesem Server
     * Findet bestehende Daten in der Datenbank oder erzeugt neuen Eintrag
     * aus dem Request
     *
     * @param request
     * @return Entity-Instanz des Servers
     */
    private Server registerChild(ParentConnectionRequest request) {
        logger.info("Received Server registration request from " + request.getChildUrl());
        Server si;

        Optional<Server> oSi = serverDao.findByUrl(request.getChildUrl());
        if (oSi.isPresent()) {
            logger.info("Bestehenden Server wiedergefunden.");
            si = oSi.get();
            if (si.getIsActive() == false) {
                si.setIsActive(true);
                serverDao.save(si);
            }
        } else {
            logger.info("Lege neuen Kind-Server an.");
            si = new Server();
            si.setGUID(guid.generateGUID());
            si.setUrl(request.getChildUrl());
        }
        serverDao.save(si);
        return si;

    }

    /**
     * Registrierung eines Clients an diesem Server
     * Findet bestehende Daten in der Datenbank oder erzeugt neuen Eintrag
     * aus dem Request
     *
     * @param request
     * @return Entity-Instanz des Clients
     */
    private Client registerClient(ParentConnectionRequest request) {
        logger.info("Received Client registration request from " + request.getChildUrl());
        Client cl;
        Optional<Client> oClient = clientDao.findByUrl(request.getChildUrl());
        if (oClient.isPresent()) {
            logger.debug("Bestehenden Client wiedergefunden.");
            cl = oClient.get();
            if (cl.getIsActive() == false) {
                cl.setIsActive(true);
                clientDao.save(cl);
            }
        } else {
            logger.debug("Erzeuge neuen Client.");
            cl = new Client();
            cl.setGUID(guid.generateGUID());
            cl.setUrl(request.getChildUrl());
        }
        clientDao.save(cl);
        return cl;

    }
}
