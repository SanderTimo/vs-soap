package de.htwsaar.vssoap.server.persistence;

import de.htwsaar.vssoap.domain.Message;
import org.hibernate.validator.constraints.NotEmpty;
import javax.persistence.*;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.GregorianCalendar;


/**
 * @author cedosw mgoebel
 */

@Entity
@Table(name ="messages",
        uniqueConstraints =  @UniqueConstraint(
        columnNames = {"GUID"}))
public class MessagePersistence extends AbstractEntity {

    @NotEmpty
    private String message;

    @NotEmpty
    private String sourceIp;

    @Column(name = "GUID")
    private String guid;

    private int rang;

    private String department;

    private String betreff;

    private String Owner;

    private String lastEmployee;

    private String type;

    private String actionType;

    private boolean approvalNeeded;

    //private BCryptPasswordEncoder encoder;

    public MessagePersistence() {

    }

    public MessagePersistence(String message, String sourceIp, int rang, String GUID, String owner, String lastEmployee, String type, String betreff,String actionType,String department) {
        this.message = message;
        this.sourceIp = sourceIp;
        this.rang = rang;
        this.guid =GUID;
        this.Owner = owner;
        this.lastEmployee = lastEmployee;
        this.type =type;
        this.betreff=betreff;
        this.actionType = actionType;
        this.department = department;
    }

    public String getActionType(){
        return actionType;
    }
    public void setActionType(String actionType){
        this.actionType =actionType;
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getSourceIp() {
        return this.sourceIp;
    }

    public void setSourceIp(String sourceIp) {
        this.sourceIp = sourceIp;
    }

    public void setRang(int rang){
        this.rang =rang;
    }
    public int getRang(){
        return this.rang;
    }
    public String getGUID() {
        return guid;
    }

    public void setGUID(String GUUID) {
        this.guid = GUUID;
    }

    public String getOwner() {
        return Owner;
    }

    public void setOwner(String owner) {
        Owner = owner;
    }

    public String getLastEmployee() {
        return lastEmployee;
    }

    public void setLastEmployee(String lastEmployee) {
        this.lastEmployee = lastEmployee;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBetreff() {
        return betreff;
    }

    public void setBetreff(String betreff) {
        this.betreff = betreff;
    }

    public boolean getApprovalNeeded() {
        return approvalNeeded;
    }

    public void setApprovalNeeded(boolean approvalNeeded) {
        this.approvalNeeded = approvalNeeded;
    }

    public String getDepartment() {return department;}

    public void setDepartment(String department) {this.department = department;}

    @Override
    public String toString(){
        return "Message: " + getGUID() + " - " + getBetreff();
    }

    public static Message toSoapMessage(MessagePersistence mp){
        Message message = new Message();
        message.setMessageText(mp.getMessage());
        message.setSourceUri(mp.getSourceIp());
        message.setType(mp.getType());
        message.setDepartment(mp.getDepartment());
        message.setOwner(mp.getOwner());
        message.setGuid(mp.getGUID());
        message.setRang(mp.getRang());
        message.setUpdatedBy(mp.getLastEmployee());
        message.setSubject(mp.getBetreff());
        message.setApprovalNeeded(mp.getApprovalNeeded());
        message.setActionType(mp.getActionType());

        try{
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(mp.getUpdated_at());
            message.setUpdatedAt(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
        }catch(DatatypeConfigurationException e){

        }

        //TODO missing : persistence:created_at zu inClient:date. fehlt in wsdl Message
        return message;
    }
}
