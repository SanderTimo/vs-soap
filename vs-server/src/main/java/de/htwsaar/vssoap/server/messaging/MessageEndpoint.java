package de.htwsaar.vssoap.server.messaging;

/**
 * Created by Timo on 27.12.2016.
 */

import de.htwsaar.vssoap.domain.CreateMessageRequest;
import de.htwsaar.vssoap.domain.CreateMessageResponse;
import de.htwsaar.vssoap.domain.Message;
import de.htwsaar.vssoap.parent.ActionType;
import de.htwsaar.vssoap.parent.MessageClient;
import de.htwsaar.vssoap.parent.PermissionType;
import de.htwsaar.vssoap.server.ConnectionHandling;
import de.htwsaar.vssoap.server.GUID;
import de.htwsaar.vssoap.server.ServerData;
import de.htwsaar.vssoap.server.forwarding.Scheduling.ScheduledSender;
import de.htwsaar.vssoap.server.forwarding.Tasks.SendTaskFactory;
import de.htwsaar.vssoap.server.forwarding.sender.SendWorkqueue;
import de.htwsaar.vssoap.server.persistence.MessageDao;
import de.htwsaar.vssoap.server.persistence.MessagePersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.Date;
import java.util.Optional;


@Endpoint
public class MessageEndpoint {
    private static final String NAMESPACE_URI = "http://htwsaar.de/vssoap/domain";
    private GUID guid = new GUID();

    @Autowired
    SendWorkqueue workqueue;

    @Autowired
    ServerData serverData;

    @Autowired
    MessageDao messageDao;

    @Autowired
    SendTaskFactory sendTaskFactory;

    @Autowired
    MessageClient transmitter;

    @Autowired
    ConnectionHandling connectionHandling;

    @Autowired
    ScheduledSender sender;

    public MessageEndpoint() {
    }

    private static Logger logger = LoggerFactory.getLogger(MessageEndpoint.class);


    /**
     * Nimmt alle verteilbaren Nachrichten entgegen:
     * neue Nachrichten, Änderungen, Delete-Flags
     * @param request
     * @return
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "createMessageRequest")
    @ResponsePayload
    public CreateMessageResponse getMessage(@RequestPayload CreateMessageRequest request) {
        CreateMessageResponse response = new CreateMessageResponse();
        MessagePersistence messagePersistence;

        logger.info("Message received.");

        if (request.getMessage().getActionType().equals(ActionType.SAVE.toString())) {


            messagePersistence = saveMessageToDatabase(request.getMessage());
            response.setMessage(MessagePersistence.toSoapMessage(messagePersistence));
        } else {
            //ActionType.DELETE
            Optional<MessagePersistence> oMessagePersistence = messageDao.findByGuid(request.getMessage().getGuid());
            if (oMessagePersistence.isPresent()) {
                sendTaskFactory.createDelteMessageTask(oMessagePersistence.get());
                messagePersistence = deleteMessageFromDatabase(request.getMessage());
                response.setMessage(MessagePersistence.toSoapMessage(messagePersistence));
                return response;
            }
            Message msg = new Message();
            msg.setActionType(ActionType.DELETE.toString());
            response = new CreateMessageResponse();
            response.setMessage(msg);
            return response;
        }
        //falls client als inaktiv markiert war: wieder aktivieren:
        connectionHandling.markActive(request.getMessage().getSourceUri());

        //weiterleitung: alle messages an alle kinder und clients, an parent falls rangMsg<rangServer

        //sende public messages zu Servern
        if (PermissionType.PUBLIC.toString().equals(messagePersistence.getType()))
            sendTaskFactory.createMessageChildrenTask(messagePersistence);

        //Send public & private messages zu Clients
        sendTaskFactory.createMessageClientsTask(messagePersistence);

        //check rang -> send to parent falls approved (durch superuser)
        if (messagePersistence.getRang() < serverData.getRang()) {
            if (!messagePersistence.getApprovalNeeded()) {
                sendTaskFactory.createMessageParentTask(messagePersistence);
            }
        }
        logger.info("Sending Response");
        return response;
    }

    /**
     * Speichert die erhaltene MessagePersistence in die Datanbank und gibt das gespeicherte Object zurück
     *
     * @param recivedMessage
     * @return
     */
    @Transactional
    private MessagePersistence saveMessageToDatabase(Message recivedMessage) {
        MessagePersistence messagePersistence = new MessagePersistence();
        if (recivedMessage.getGuid() != null) {
            Optional<MessagePersistence> m = messageDao.findByGuid(recivedMessage.getGuid());
            if (m.isPresent()) {  // message mit guid schon in der datenbank vorhanden
                messagePersistence = m.get();
                //skip messages if a newer version exists
                Date reveivedUpdatedAt = recivedMessage.getUpdatedAt().toGregorianCalendar().getTime();
                if (reveivedUpdatedAt.compareTo(messagePersistence.getUpdated_at()) < 0) {
                    //return current db entry instead of updating it
                    return messagePersistence;
                }
            } else {
                //guid wurde in anderem Server erzeugt, liegt hier aber noch nicht vor
                messagePersistence.setGUID((recivedMessage.getGuid()));
            }


        } else {
            //neue message
            messagePersistence.setGUID(guid.generateGUID());
        }
        messagePersistence.setDepartment(recivedMessage.getDepartment());
        messagePersistence.setBetreff(recivedMessage.getSubject());
        messagePersistence.setMessage(recivedMessage.getMessageText());
        messagePersistence.setLastEmployee(recivedMessage.getOwner());
        messagePersistence.setOwner(recivedMessage.getOwner());
        messagePersistence.setRang(recivedMessage.getRang());
        messagePersistence.setSourceIp(recivedMessage.getSourceUri());
        messagePersistence.setType(recivedMessage.getType());
        messagePersistence.setActionType(recivedMessage.getActionType());
        messagePersistence.setApprovalNeeded(recivedMessage.isApprovalNeeded());
        messagePersistence = messageDao.save(messagePersistence);
        return messagePersistence;
    }

    /**
     * Löscht Message aus der Datenbank, und liefert Anweisung um Löschen
     * in anderen Servern/Clients die verteilt werden soll zurück
     * @param recivedMessage
     * @return
     */
    private MessagePersistence deleteMessageFromDatabase(Message recivedMessage) {
        MessagePersistence messagePersistence = new MessagePersistence();
        ;
        if (recivedMessage.getGuid() != null) {
            Optional<MessagePersistence> m = messageDao.findByGuid(recivedMessage.getGuid());
            if (m.isPresent()) {
                messagePersistence = m.get();
            }
            messageDao.deleteByGuid(recivedMessage.getGuid());
        }
        messagePersistence.setActionType("DELETE");
        return messagePersistence;
    }
}