package de.htwsaar.vssoap.server.persistence;


import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * @author cedosw mgoebel
 */
@Transactional
public interface CustomUserDao extends CrudRepository<CustomUser, Integer> {
    Optional<CustomUser> findByUsername(String username);
}
