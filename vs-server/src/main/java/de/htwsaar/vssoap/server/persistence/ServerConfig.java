package de.htwsaar.vssoap.server.persistence;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;

/**
 * @author JulianW
 */

@Entity
@Table(name = "config")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public class ServerConfig extends MessageTarget{

    private String department;

    private String parent;

    private int rang;

    public String getDepartment(){return department;}

    public void setDepartment(String department){this.department=department;}

    public int getRang() {
        return rang;
    }

    public void setRang(int rang) {
        this.rang=rang;
    }

    public String getParentUrl(){
        return this.parent;
    }

    public void setParentUrl(String parent){
        this.parent=parent;
    }

    public Boolean getParentActive() {
        return isActive;
    }

    public void setParentActive(Boolean parentActive) {
        isActive = parentActive;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName() + ":");
        sb.append("URL: " + getUrl() + ", ");
        sb.append("Parent: " + getParentUrl()+", ");
        sb.append("Rang: " + getRang() + ", ");
        return sb.toString();
    }

}
