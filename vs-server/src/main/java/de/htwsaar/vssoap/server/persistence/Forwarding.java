package de.htwsaar.vssoap.server.persistence;

import de.htwsaar.vssoap.parent.ConnectionType;
import de.htwsaar.vssoap.parent.SoapMessageType;

import javax.persistence.*;

/**
 * Enthält die Informationen zum Weiterleiten einer Nachricht an ein Ziel
 * Persistiert, damit im Fall eines Absturzes nichts verloren geht
 * Created by Julian on 23.03.2017.
 */
@Entity
@Table(uniqueConstraints =  @UniqueConstraint(
        columnNames = {"SoapMessageId","RecipientId"}))
public class Forwarding extends AbstractEntity {

    /*
    * kein Foreign Key, da Forwards an Kinder etc., dass Delte durchgeführt werden soll
    * auf nachdem Message hier gelöscht wurde noch möglich sein muss
     */
    private String soapMessageId;

    private String recipientId;

    @Enumerated(EnumType.STRING)
    private SoapMessageType soapMessageType;

    @Enumerated(EnumType.STRING)
    private ConnectionType connectionType;

    public ConnectionType getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(ConnectionType connectionType) {
        this.connectionType = connectionType;
    }

    public String getRecipientId() {  return recipientId;   }

    public void setRecipientId(String recipientId) {
        this.recipientId = recipientId;
    }

    public SoapMessageType getSoapMessageType() {
        return this.soapMessageType;
    }

    public void setSoapMessageType(SoapMessageType type) {
        this.soapMessageType = type;
    }

    public String getSoapMessageId() {
        return this.soapMessageId;
    }

    public void setSoapMessageId(String id) {
        this.soapMessageId = id;
    }

    @Override
    public String toString() {
        return "Forward: " + soapMessageId + " an " + recipientId;
    }


}
