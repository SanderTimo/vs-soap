package de.htwsaar.vssoap.server.persistence;


        import org.springframework.data.repository.CrudRepository;

        import javax.transaction.Transactional;
        import java.util.List;
        import java.util.Optional;

/**
 * Created by Julian on 15.01.2017.
 */
@Transactional
public interface ServerDao extends CrudRepository<Server, Integer> {
    Optional<Server> findByUrl(String Url);
    Optional<Server> findByGuid(String id);
    Optional<List<Server>> findByIsActive(Boolean isActive);

}
