package de.htwsaar.vssoap.server.messaging;


/**
 * Eigene Exceptionklasse für Fehler in Message Contents.
 *
 * Created by Julian on 24.03.2017.
 */
public class MessagingException extends Exception {

    public MessagingException(Throwable error){
        super(error);
    }

    public MessagingException(String errorMsg, Throwable error){
        super(errorMsg, error);
    }

    public MessagingException(String errorMsg){
        super(errorMsg);
    }
}
