package de.htwsaar.vssoap.server.persistence;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.remoting.caucho.BurlapClientInterceptor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Julian on 15.01.2017.
 */
@MappedSuperclass
public abstract class MessageTarget extends AbstractEntity {

    @Column(unique = true)
    @NotBlank
    private String url;

    @Column(name = "GUID")
    @NotBlank
    private String guid;

    public MessageTarget(){

    }

    public MessageTarget(String url, String guid){
        this.url=url;
        this.guid=guid;

    }

    /**
     * Server wird inaktiv gesetzt, wenn das senden einer message zu ihm Fehlschlägt
     * Aktiviert: wenn er selbst zum sever sendet
     * oder wenn der schedule task wieder eine verbindng aufbauen kann
     */
    @Column
    @ColumnDefault("true")
    public boolean isActive = true;

    /**
     * persistenz für Ausgang der weiterleitung
     *
     * während
     */
  /*  @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    public List<MessagePersistence> scheduledMessages = new ArrayList<>();
*/

    public void setIsActive(boolean isActive){
        this.isActive=isActive;
    }


    public boolean getIsActive(){
        return this.isActive;
    }

    public String getUrl() {
        return url;
    }


    public String getGUID() {
        return guid;
    }

    public void setGUID(String GUUID) {
        this.guid = GUUID;
    }


    public void setUrl(String url) {
        this.url = url;
    }

}
