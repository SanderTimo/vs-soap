package de.htwsaar.vssoap.server.forwarding.sender;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;


/**
 * Workqueue zum Abarbeitgen von Tasks außerhalb des Haupt-Threads
 * Insbesondere werden Messageweiterleitungen in die Queue eingestellt
 *
 * @see ThreadPoolTaskExecutor
 * @author JulianW
 */
@Component
@EnableAsync
public class SendWorkqueue implements AsyncConfigurer {


    protected static Logger logger = LoggerFactory.getLogger(SendWorkqueue.class);

    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(4);
        executor.setMaxPoolSize(10);
        executor.setQueueCapacity(10);
        executor.initialize();
        return executor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() { return null; }

    private List<Runnable> workQueue;

    public SendWorkqueue(){
        //add und remove müssen hier synchronized sein
        this.workQueue= Collections.synchronizedList(new ArrayList<>());
    }

    /**
     * Tasks in die Workqueue einstellen
     * @param task
     */
    public void addTask(Runnable task){
        if(task==null) return;
        logger.debug("Neuen Task in workqueue aufgenommen." + workQueue.size() + " verbleibend");
        workQueue.add(task);
        if(workQueue.size()==1){
            processTasks();
        }
    }

    /**
     * Abarbeitung der Workqueue
     */
    @Async
    private void processTasks(){
        if(workQueue.size()==0 ) return;
        while(workQueue.size()>0) {
            logger.debug("Abarbeitung der Workqueue: entnehme item");
            Runnable task = workQueue.remove(0);
            getAsyncExecutor().execute(task);
        }
    }
}
