package de.htwsaar.vssoap.server.forwarding.Scheduling;

import de.htwsaar.vssoap.parent.ConnectionType;
import de.htwsaar.vssoap.parent.MessageClient;
import de.htwsaar.vssoap.server.ConnectionHandling;
import de.htwsaar.vssoap.server.ServerData;
import de.htwsaar.vssoap.server.forwarding.Tasks.SendSingleMessage;
import de.htwsaar.vssoap.server.forwarding.sender.SendWorkqueue;
import de.htwsaar.vssoap.server.persistence.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;
import java.util.Optional;

/**
 * Regelmäßig werden für alle Ziele alle Weiterleitungen in die
 * Workqueue hinzugefügt
 *
 * Created by Julian on 24.03.2017.
 */
@Configuration
@EnableAsync
@EnableScheduling
public class ScheduledSender {


    private static Logger logger = LoggerFactory.getLogger(ScheduledSender.class);

    @Autowired
    ClientDao clientDao;

    @Autowired
    ServerDao serverDao;

    @Autowired
    ForwardingDao forwardingDao;

    @Autowired
    ServerData serverData;

    @Autowired
    SendWorkqueue workqueue;

    @Autowired
    MessageDao messageDao;

    @Autowired
    MessageClient messageClient;

    @Autowired
    ConnectionHandling connectionHandling;


    /**
     * Ermittelt alle als aktiv gekennzeichneten Kinder/Parent/Server
     * nur für diese ist das autmatisierte Senden aktiviert
     */
    @Scheduled(initialDelay = 1000, fixedDelay = 1500)
    public void resendToActive() {
        logger.debug("Scheduled forwarding to all active targets");
        Optional<List<Client>> activeClientsOpt = clientDao.findByIsActive(true);
        if (activeClientsOpt.isPresent()) {
            activeClientsOpt.get().forEach((client) -> {
                addToWorkqueue(client.getGUID());
            });
        }
        Optional<List<Server>> activeServersOpt = serverDao.findByIsActive(true);
        if (activeServersOpt.isPresent()) {
            activeServersOpt.get().forEach((server) -> {
                addToWorkqueue(server.getGUID());
            });
        }
        if (serverData.isParentActive()) {
            addToWorkqueue(serverData.readConfigDb().getGUID());
        }
    }

    @Async
    private void addToWorkqueue(String guid) {

        Optional<List<Forwarding>> forwardings = forwardingDao.findByRecipientId(guid);
        if (forwardings.isPresent()) {
            forwardings.get().forEach((forwarding) -> {
                SendSingleMessage sender = new SendSingleMessage(forwarding);
                hookupSender(sender);
                workqueue.addTask(sender);
            });
        }
    }

    private void hookupSender(SendSingleMessage sender) {
        sender.hookup(messageClient, forwardingDao, connectionHandling, messageDao, serverDao, clientDao, serverData);
    }

}
