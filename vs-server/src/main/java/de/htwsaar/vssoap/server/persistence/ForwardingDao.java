package de.htwsaar.vssoap.server.persistence;

import de.htwsaar.vssoap.parent.ConnectionType;
import de.htwsaar.vssoap.parent.SoapMessageType;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * Created by Julian on 24.03.2017.
 */

@Transactional
public interface ForwardingDao  extends CrudRepository<Forwarding, Integer> {

    Optional<List<Forwarding>> findBySoapMessageType(SoapMessageType soapMessageType);
    Optional<List<Forwarding>> findByRecipientId(String id);
    Long deleteByRecipientId(String recipientid);
    Long deleteByConnectionType(ConnectionType type);
    Long deleteBySoapMessageId(String soapGuid);
    Optional<Forwarding> findOneByRecipientIdAndSoapMessageId(String recipientId, String SoapMessageId);

}
