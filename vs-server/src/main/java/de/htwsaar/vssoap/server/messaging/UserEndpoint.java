package de.htwsaar.vssoap.server.messaging;

import de.htwsaar.vssoap.domain.GetCreateUserRequest;
import de.htwsaar.vssoap.domain.GetCreateUserResponse;
import de.htwsaar.vssoap.server.persistence.CustomUser;
import de.htwsaar.vssoap.server.persistence.CustomUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 * @author cedosw (27.03.2017)
 */
@Endpoint
public class UserEndpoint {
    private static final String namespace = "http://htwsaar.de/vssoap/domain";

    @Autowired
    private CustomUserDao userDao;

    @PayloadRoot(namespace = namespace, localPart = "getCreateUserRequest")
    @ResponsePayload
    public GetCreateUserResponse createUser(@RequestPayload GetCreateUserRequest req) {
        GetCreateUserResponse response = new GetCreateUserResponse();
        response.setCreated(false);
        CustomUser user = new CustomUser(req.getUsername(), req.getPassword(), req.getRole());
        userDao.save(user);
        if (user.getId() != 0) {response.setCreated(true);}
        return response;
    }
}
