package de.htwsaar.vssoap.server.persistence;

import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author cedosw mgoebel
 */
@Transactional
public interface MessageDao extends CrudRepository<MessagePersistence, Integer> {
        MessagePersistence save(MessagePersistence messagePersistence);

        Optional<List<MessagePersistence>> findByType(String type);
        Optional<MessagePersistence> findByGuid(String guid);
        Optional<List<MessagePersistence>> findAllByRangLessThanAndSourceIpNot(int rang, String sourceip);
        void deleteByGuid(String guid);

}

