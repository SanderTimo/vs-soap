package de.htwsaar.vssoap.server;

import de.htwsaar.vssoap.parent.ConnectionType;
import de.htwsaar.vssoap.server.persistence.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Markiert Messageziele als inaktiv, wenn nicht erreichbar
 * bzw. aktiv, wenn Verbindung wieder möglich ist
 * <p>
 * Created by Julian on 24.03.2017.
 */
@Component
public class ConnectionHandling {

    private static Logger logger = LoggerFactory.getLogger(ConnectionHandling.class);

    @Autowired
    private ServerDao serverDao;

    @Autowired
    private ClientDao clientDao;

    @Autowired
    private ServerData serverData;

    @Autowired
    private ForwardingDao forwardingDao;


    /**
     * Schlagen Messages an ein Ziel fehl wird es als inaktiv markiert
     * dadurch werden Weiterleitungen an dieses Ziel nicht rausgesendet.
     * Inaktive Ziele werden Regelmäßig vom AvailabilityRequest angesprochen
     *
     * @param id
     * @param connectionType
     * @return
     */
    public boolean markInactive(int id, ConnectionType connectionType) {
        switch (connectionType) {
            case CLIENT:
                Client client = clientDao.findOne(id);
                if (client == null) break;
                client.setIsActive(false);
                clientDao.save(client);
                break;

            case SERVER:
                Server server = serverDao.findOne(id);
                if (server == null) break;
                server.setIsActive(false);
                serverDao.save(server);
                break;
            case PARENT:
                serverData.setParentActive(false);
                return false;
        }
        return true;
    }

    /**
     * Markiert einen als offline erkannten Server/client/parent als wieder verfügbar
     * Dadurch wird das Senden zwischenzeitlich aufgetretene Messages für das Ziel
     * angestoßen
     *
     * @param uri
     * @param
     * @return
     */
    public void markActive(String uri) {
        if (uri == null) return;
        if (uri.equals(serverData.getParentUrl())) {
            serverData.setParentActive(true);
            return;
        }
        Optional<Client> client = clientDao.findByUrl(uri);
        if (client.isPresent() && !client.get().getIsActive()) {
            client.get().setIsActive(true);
            clientDao.save(client.get());
            logger.info("Client wieder aktiv:" + client.get().getUrl());
            return;
        }
        Optional<Server> server = serverDao.findByUrl(uri);
        if (server.isPresent() && !server.get().getIsActive()) {
            server.get().setIsActive(true);
            serverDao.save(server.get());
            logger.info("Server wieder aktiv:" + server.get().getUrl());
            return;
        }
    }


    /**
     * wenn ganz aus der db gelöscht wird
     * werden forwardings mitgelöscht
     *
     * @param url            des clients/servers
     * @param connectionType
     * @return
     */
    public boolean delete(String url, ConnectionType connectionType) {
        switch (connectionType) {
            case CLIENT:
                Optional<Client> client = clientDao.findByUrl(url);
                if (client.isPresent())
                    forwardingDao.deleteByRecipientId(client.get().getGUID());
                clientDao.delete(client.get());
                logger.info("Client abgemeldet:" + client.get().getUrl());
                break;

            case SERVER:
                Optional<Server> server = serverDao.findByUrl(url);
                if (server.isPresent()) {
                    forwardingDao.deleteByRecipientId(server.get().getGUID());
                    serverDao.delete(server.get());
                    logger.info("Server abgemeldet" + server.get().getUrl());
                }
                break;
            case PARENT:
                serverData.setParentActive(false);
                forwardingDao.deleteByConnectionType(ConnectionType.PARENT);
                logger.info("Parent abgemeldet:" + serverData.getParentUrl());
                return false;
        }
        return true;
    }


}
