package de.htwsaar.vssoap.server.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.soap.security.wss4j2.Wss4jSecurityInterceptor;
import org.springframework.ws.soap.security.wss4j2.callback.SpringSecurityPasswordValidationCallbackHandler;

import java.util.List;

/**
 * @author cedosw (22.03.2017)
 */
@EnableWs
@Configuration
public class SoapServerConfig extends WsConfigurerAdapter {

    @Autowired
    private UserService userService;

    @Autowired
    private Wss4jSecurityInterceptor interceptor;

    @Bean
    public SpringSecurityPasswordValidationCallbackHandler securityCallbackHandler() {
        SpringSecurityPasswordValidationCallbackHandler callbackHandler = new SpringSecurityPasswordValidationCallbackHandler();
        callbackHandler.setUserDetailsService(userService);
        return callbackHandler;
    }

    @Override
    public void addInterceptors(List interceptors) {
        interceptors.add(interceptor);
    }

}
