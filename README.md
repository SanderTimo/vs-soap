# README vs-soap #

* Das Projekt für die Vorlesung Verteilte Systeme 2016/17
* Verteiltes System (hierarchisch) für Anzeigetafeln mit SOAP
* Ziel des Projektes ist die Abschlussnote 1.0
* Treffen im Teamspeak (IP `ts-3.net:16977`, Password `ddosprotected`)

### Setup ###

*Forthcoming*

### Contribution guidelines ###

* Feature Branch erstellt (Branchname: `feature/<feature-name>`)
* Tests geschrieben und geprüft ob alle Tests laufen
* mit meinem Partner Code Review gemacht
* das zugehörige Issue geschlossen und in Commits darauf verwiesen *(Info dazu gibt es [hier](https://confluence.atlassian.com/bitbucket/resolve-issues-automatically-when-users-push-code-221451126.html))*
* Pull Request erstellt (Info zu Pull Requests [hier](https://confluence.atlassian.com/bitbucket/work-with-pull-requests-223220593.html))

### Project guidelines ###

Ich ...

* bin bei jedem Treffen (Plenum/Team) anwesend.
* erledige die Issues fristgerecht mit meinem Partner.
* halte mich an die 'Contribution guidelines'.
* dokumentiere die Zeit, die ich an dem Projekt arbeite.


 
*Jede Missachtung einer Regel führt zu einer Verwarnung. Bei 3 Verwarnungen wird Herr Sauer benachrichtigt und es erfolgt ein Projektausschluss.*